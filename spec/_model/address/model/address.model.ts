import { ObjectShell } from '@taedr/requesters/core';
import { Coords } from '../../parts/coords.model';

export const ADDRESS = Symbol(`ADDRESS`);


export class Address {
   public country = ``;
   public city = ``;
   public coords = new Coords();
}


