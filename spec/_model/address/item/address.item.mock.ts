import { AShellMock } from '@taedr/requesters/core';
import { Address } from '../model/address.model';


export class AddressItemMock extends AShellMock {
   constructor(

   ) {
      super(true, 100);
   }


   public async getData(_: null, path: string) {
      const address = new Address();

      return address;
   }
}