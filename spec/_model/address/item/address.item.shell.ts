import { ObjectShell } from '@taedr/requesters/core';
import { Address } from '../model/address.model';
import { AddressItemMock } from './address.item.mock';

export class AddressShell extends ObjectShell<null, Address> {
   constructor(
      readonly userId: number,
   ) {
      super({
         id: `User ${userId} Address`,
         model: Address,
         path: `users/${userId}/address`,
         ttlSec: 2,
         requester: null,
         mock: new AddressItemMock(),
      })
   }

   public load() {
      return super.load(null);
   }
}