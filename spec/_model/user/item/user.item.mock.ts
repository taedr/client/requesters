import { AShellMock } from '@taedr/requesters/core';
import { IdNum, IMap } from '@taedr/utils';
import { IUser } from '../model/user.interface';
import { User } from '../model/user.model';


export const USER_ITEM_MOCK: IMap<IUser> = {
   1: {
      id: 1,
      name: `Ipolit`
   },
   
}


export class UserItemMock extends AShellMock {
   constructor(
      getData?: (filter: IdNum) => Promise<IUser>,
      delay = 10,
   ) {
      super(true, delay);
      if (getData) this.getData = getData;
   }


   public async getData({ id }: IdNum): Promise<any> {
      const user = new User();
      user.id = id;

      return user;
   }
}