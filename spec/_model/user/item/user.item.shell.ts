import { IdObjectShell } from '@taedr/requesters/core';
import { IdNum } from '@taedr/utils';
import { USERS_TTL_MS } from '../model/user.constants';
import { UserItemMock } from './user.item.mock';
import { IUser } from '../model/user.interface';
import { User } from '../model/user.model';


export class UserItemShell extends IdObjectShell<IdNum, User> {
   constructor(
      getData?: (filter: IdNum) => Promise<any>,
      delay?: number,
   ) {
      super({
         id: `Users`,
         model: User,
         path: `users`,
         requester: null,
         mock: new UserItemMock(getData, delay),
         ttlSec: USERS_TTL_MS / 1000,
         
      });
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public load(filter: IdNum = { id: 1 }) {
      return super.load(filter)
   }

   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
}