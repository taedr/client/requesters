import { ShellActionsSync } from '@taedr/requesters/core';
import { User } from '../model/user.model';
import { USER_ADD } from './add/user.add.shell';
import { USER_DELETE } from './delete/user.delete.shell';
import { USER_UPDATE } from './update/user.update.shell';


export const USER_ACTIONS_SYNC = new ShellActionsSync<User>({
   add: USER_ADD.success,
   delete: USER_DELETE.success,
   update: USER_UPDATE.success,
}); 


