import { AShellMock } from '@taedr/requesters/core';
import { IUserUpdate } from './user.update.model';

export class UserUpdateMock extends AShellMock {
   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData(payload: IUserUpdate, path: string[]) {
      const id = +path.slice(-1)[0];


      return payload;
   }
}
