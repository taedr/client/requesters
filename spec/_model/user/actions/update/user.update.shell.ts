import { ObjectAction } from '@taedr/requesters/core';
import { User } from '../../model/user.model';
import { IUserUpdate } from './user.update.model';
import { UserUpdateMock } from './user.update.mock';

export const USER_UPDATE = new ObjectAction<IUserUpdate, IUserUpdate>(() => ({
   id: `User Update`,
   model: User,
   ttlSec: 2,
   isDebug: true,
   requester: null,
   mock: new UserUpdateMock(true),
   path: `users`,
}));