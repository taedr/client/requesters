import { TDeepPartial } from '@taedr/utils';
import { User } from '../../model/user.model';

export interface IUserUpdate extends TDeepPartial<User> {
   id: number;
}
