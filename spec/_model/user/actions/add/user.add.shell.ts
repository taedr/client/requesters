import { ObjectAction } from '@taedr/requesters/core';
import { User } from '../../model/user.model';
import { UserAddMock } from './user.add.mock';
import { IUserAdd } from './user.add.model';

export const USER_ADD = new ObjectAction<IUserAdd, User>(() => ({
   id: `User Add`,
   model: User,
   ttlSec: 2,
   isDebug: true,
   requester: null,
   mock: new UserAddMock(true),
   path: `users`,
}));