import { AShellMock } from '@taedr/requesters/core';
import { IUserAdd } from './user.add.model';

export class UserAddMock extends AShellMock {
   protected static _id = 0;
   
   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData(user: IUserAdd) {
      return {
         ...user,
         id: --UserAddMock._id
      };
   }
}
