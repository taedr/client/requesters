import { AShellMock } from '@taedr/requesters/core';
import { IUserDelete } from './user.delete.model';

export class UserDeleteMock extends AShellMock {
   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData(payload: null, path: string[]) {
      const id = +path.slice(-1)[0];

      return { id };
   }
}
