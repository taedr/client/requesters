import { ObjectAction } from '@taedr/requesters/core';
import { IdNum } from '@taedr/utils';
import { User } from '../../model/user.model';
import { UserDeleteMock } from './user.delete.mock';
import { IUserDelete } from './user.delete.model';

export const USER_DELETE = new ObjectAction<User, User>(() => ({
   id: `User Delete`,
   model: User,
   ttlSec: 2,
   isDebug: true,
   requester: null,
   mock: new UserDeleteMock(true),
   path: `users`,
}));