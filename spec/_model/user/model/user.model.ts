import { TDeepPartial } from '@taedr/utils';
import { IUserUpdate, USER_DELETE, USER_UPDATE } from '../actions';
import { IUser } from './user.interface';
import { AUserRelation } from './user.relation';



export class User extends AUserRelation implements IUser {
   public id = 0;
   public name = ``;

   public update(values: TDeepPartial<User>) {
      return USER_UPDATE.perform({ id: this.id, ...values}).state;
   }


   public delete() {
      return USER_DELETE.perform(this).state;
   }
} 