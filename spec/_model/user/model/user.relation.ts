import { JTC } from '@taedr/jsontoclass';
import { AddressShell } from '../../address/item/address.item.shell';
import { ADDRESS } from '../../address/model/address.model';
import { PostListShardShell } from '../../post/list/post.list.shell';
import { POSTS } from '../../post/model/post.model';

export const USER = Symbol(`USER`);
export const USERS = Symbol(`USERS`);


export abstract class AUserRelation {
   public id = 0;
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public get posts() { return this[POSTS]; }
   public get address() { return this[ADDRESS]; }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   private [POSTS]: PostListShardShell;
   private [ADDRESS]: AddressShell;
   // ---------------------------------------------
   //                Init
   // ---------------------------------------------
   static [JTC.INIT](relation: AUserRelation) {
      relation[POSTS] = new PostListShardShell(relation.id);
      relation[ADDRESS] = new AddressShell(relation.id);
   }
}
