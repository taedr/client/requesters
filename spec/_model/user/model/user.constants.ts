import { ShellValuesSync } from '@taedr/requesters/core';
import { User } from './user.model';

export const USERS_PACK_SIZE = 3;
export const USERS_TTL_MS = 100;