import { TPosition } from '@taedr/arrays';
import { ArrayCache, ArrayShell, ShellCache } from '@taedr/requesters/core';
import { IListFilter } from '../../utils/list.filter';
import { User } from '../model/user.model';
import { USERS_PACK_SIZE, USERS_TTL_MS } from '../model/user.constants';
import { UserListMock } from './users.list.mock';
import { IUserAdd, USER_ACTIONS_SYNC, USER_ADD } from '../actions';


export class UserListShell extends ArrayShell<IListFilter, User> {
   constructor(
   ) {
      super({
         id: `Users`,
         model: User,
         packSize: USERS_PACK_SIZE,
         path: [`users`],
         requester: null,
         mock: new UserListMock(),
         ttlSec: USERS_TTL_MS / 1000,
         actions: USER_ACTIONS_SYNC,
         cache: { size: 3, entryTtlSec: 10 }
      });
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public load(filter: IListFilter = { page: 1 }) {
      return super.load(filter)
   }


   public merge(filter: IListFilter = {
      page: this.state.merges + 1
   }, position?: TPosition) {
      return super.merge(filter, position);
   }



   public add(user: IUserAdd) {
      // USER_ADD.perform(user).
      // this.
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
} 