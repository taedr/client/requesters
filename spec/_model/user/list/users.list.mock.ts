import { AShellMock } from '@taedr/requesters/core';
import { FN_NULL, FN_PACK, IMap, Promised } from '@taedr/utils';
import { IListFilter } from '../../utils/list.filter';
import { IUser } from '../model/user.interface';
import { User } from '../model/user.model';
import { USERS_PACK_SIZE } from '../model/user.constants';


export const USER_LIST_MOCK: IMap<IUser[]> = {
   1: [
      { id: 1, name: `Vasek` },
      { id: 2, name: `Katya` },
      { id: 3, name: `Vasil` },
   ],
   2: [
      { id: 4, name: `Evgen` },
      { id: 5, name: `Ibragim` },
      { id: 6, name: `Evdokia` },
   ],
   3: [
      { id: 7, name: `Ipolit` },
      { id: 8, name: `Maria` },
      { id: 9, name: `Grigorii` },
   ],
   4: [
      { id: 10, name: `Zahar` },
   ],
   5: [],
   6: null,
   7: <any>{},
   8: <any[]>[
      { id: `1`, name: `Vasek` },
      undefined,
      { id: 2, name: 12 },
      { id: null, name: `Vasil` },
      null,
   ],
};


export class UserListMock extends AShellMock {
   constructor(
   ) {
      super(true, 10);
   }


   public async getData({ page }: IListFilter, path: string[]): Promise<any> {
      switch(page) {
         case 9: return new Promise(FN_NULL);
         case 10: throw new Error(`Error`);
         default: return USER_LIST_MOCK[page];
      }
   }
}