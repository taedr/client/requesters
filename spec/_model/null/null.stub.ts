import { AShellMock } from '@taedr/requesters/core';

export class NullStub extends AShellMock {
   constructor(
      getData?: () => Promise<any>,
   ) {
      super(true, 10);
      if (getData) this.getData = getData;
   }

   
   public async getData() {
      return null;
   }
}