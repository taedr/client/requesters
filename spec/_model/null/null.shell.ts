import { ObjectShell } from '@taedr/requesters/core';
import { NullStub } from './null.stub';

export class NullShell extends ObjectShell<null, null> {
   constructor(
      getData?: () => Promise<any>,
   ) {
      super({
         id: `nullable`,
         model: null,
         path: ``,
         requester: null,
         mock: new NullStub(getData),
         ttlSec: 2,
      })
   }

   
   public load() {
      return super.load(null);
   }
};