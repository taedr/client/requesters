import { TPosition } from '@taedr/arrays';
import { ArrayShell } from '@taedr/requesters/core';
import { IListFilter } from '../../utils/list.filter';
import { POSTS_TTL_MS, POST_LIST_PACK_SIZE } from '../model/post.constants';
import { Post } from '../model/post.model';
import { PostListMock } from './post.list.mock';


export class PostListShell extends ArrayShell<IListFilter, Post> {
   constructor(
      getData?: (filter: IListFilter, path: string[]) => Promise<Post[]>,
   ) {
      super({
         id: `Users`,
         model: Post,
         packSize: POST_LIST_PACK_SIZE,
         path: `users`,
         requester: null,
         mock: new PostListMock(getData),
         ttlSec: POSTS_TTL_MS / 1000,
      });
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public load(filter: IListFilter = { page: 1 }) {
      return super.load(filter)
   }


   public merge(filter: IListFilter = {
      page: this.state.merges + 1
   }, position?: TPosition) {
      return super.merge(filter, position);
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
}


export class PostListShardShell extends PostListShell {
   constructor(
      readonly userId: number
   ) {
      super();
   }
}