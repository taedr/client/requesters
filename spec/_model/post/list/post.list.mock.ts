import { AShellMock } from '@taedr/requesters/core';
import { FN_PACK, IMap } from '@taedr/utils';
import { IListFilter } from '../../utils/list.filter';
import { POST_LIST_PACK_SIZE } from '../model/post.constants';
import { IPost } from '../model/post.interface';
import { Post } from '../model/post.model';


export const POST_LIST_MOCK: IMap<IPost[]> = {
   1: [
      { id: 1, text: `Text 1` },
      { id: 2, text: `Text 2` },
      { id: 3, text: `Text 3` },
   ],
   2: [
      { id: 4, text: `Text 4` },
      { id: 5, text: `Text 5` },
      { id: 6, text: `Text 6` },
   ],
   3: [
      { id: 7, text: `Text 7` },
   ]
}


export class PostListMock extends AShellMock {
   constructor(
      getData?: (filter: IListFilter, path: string[]) => Promise<Post[]>,
   ) {
      super(true, 10);
      if (getData) this.getData = getData;
   }


   public async getData({ page }: IListFilter, path: string[]) {
      return POST_LIST_MOCK[page];
   }
}