import { ObjectAction } from '@taedr/requesters/core';
import { Post } from '../../model/post.model';
import { IPostDelete } from './post.delete.model';
import { PostDeleteMock } from './post.delete.mock';

export const POST_DELETE = new ObjectAction<IPostDelete, IPostDelete>(() => ({
   id: `Post Delete`,
   model: Post,
   ttlSec: 2,
   isDebug: true,
   requester: null,
   mock: new PostDeleteMock(true),
   path: `users`,
}));