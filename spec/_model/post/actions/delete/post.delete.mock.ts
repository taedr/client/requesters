import { AShellMock } from '@taedr/requesters/core';
import { IPostDelete } from './post.delete.model';

export class PostDeleteMock extends AShellMock {
   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData(payload: null, path: string) {
      const id = +path.split(`/`).slice(-1)[0];

      return { id };
   }
}
