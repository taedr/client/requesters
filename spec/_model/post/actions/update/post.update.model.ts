import { TDeepPartial } from '@taedr/utils';
import { Post } from '../../model/post.model';

export interface IPostUpdate extends TDeepPartial<Post> {
   id: number;
}
