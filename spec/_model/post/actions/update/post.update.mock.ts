import { AShellMock } from '@taedr/requesters/core';
import { IPostUpdate } from './post.update.model';

export class PostUpdateMock extends AShellMock {
   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData(payload: IPostUpdate, path: string) {
      const id = +path.split(`/`).slice(-1)[0];


      return payload;
   }
}
