import { ObjectAction } from '@taedr/requesters/core';
import { Post } from '../../model/post.model';
import { IPostUpdate } from './post.update.model';
import { PostUpdateMock } from './post.update.mock';

export const POST_UPDATE = new ObjectAction<IPostUpdate, IPostUpdate>(() => ({
   id: `Post Update`,
   model: Post,
   ttlSec: 2,
   isDebug: true,
   requester: null,
   mock: new PostUpdateMock(true),
   path: `users`,
}));