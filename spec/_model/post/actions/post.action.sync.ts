import { ShellActionsSync } from '@taedr/requesters/core';
import { Post } from '../model/post.model';
import { POST_ADD } from './add/post.add.shell';
import { POST_DELETE } from './delete/post.delete.shell';
import { POST_UPDATE } from './update/post.update.shell';


export const POST_ACTIONS_SYNC = new ShellActionsSync<Post>({
   add: POST_ADD.success,
   delete: POST_DELETE.success,
   update: POST_UPDATE.success,
}); 


