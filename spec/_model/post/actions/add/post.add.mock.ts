import { AShellMock } from '@taedr/requesters/core';
import { Post } from '../../model/post.model';

export class PostAddMock extends AShellMock {
   protected static _id = 0;
   
   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData(user: Post) {
      if (!!user.id) {
         throw new Error(`User can't have setted id while adding`);
      }

      // user.id = max(usersStub.value, ({ id }) => id).max + 1;
      // usersStub.add(user);

      // return user;
      user.id = --PostAddMock._id;

      return user;
   }
}
