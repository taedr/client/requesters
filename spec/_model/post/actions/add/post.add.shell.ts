import { ObjectAction } from '@taedr/requesters/core';
import { Post } from '../../model/post.model';
import { PostAddMock } from './post.add.mock';
import { IPostAdd } from './post.add.model';

export const POST_ADD = new ObjectAction<IPostAdd, Post>(() => ({
   id: `Post Add`,
   model: Post,
   ttlSec: 2,
   isDebug: true,
   requester: null,
   mock: new PostAddMock(true),
   path: `users`,
}));