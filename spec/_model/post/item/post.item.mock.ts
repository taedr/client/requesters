import { AShellMock } from '@taedr/requesters/core';
import { IdNum } from '@taedr/utils';
import { Post } from '../model/post.model';


export class PostItemMock extends AShellMock {
   constructor(
      getData?: (filter: IdNum) => Promise<Post>,
      delay = 10,
   ) {
      super(true, delay);
      if (getData) this.getData = getData;
   }


   public async getData({ id }: IdNum) {
      const user = new Post();
      user.id = id;

      return user;
   }
}