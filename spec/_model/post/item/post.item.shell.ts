import { IdObjectShell } from '@taedr/requesters/core';
import { IdNum } from '@taedr/utils';
import { POSTS_TTL_MS } from '../model/post.constants';
import { Post } from '../model/post.model';
import { PostItemMock } from './post.item.mock';


export class PostItemShell extends IdObjectShell<IdNum, Post> {
   constructor(
      getData?: (filter: IdNum) => Promise<Post>,
      delay?: number,
   ) {
      super({
         id: `Users`,
         model: Post,
         path: `users`,
         requester: null,
         mock: new PostItemMock(getData, delay),
         ttlSec: POSTS_TTL_MS / 1000,
      });
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public load(filter: IdNum = { id: 1 }) {
      return super.load(filter)
   }

   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
}