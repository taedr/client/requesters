import { IPost } from './post.interface';

export const POSTS = Symbol(`POSTS`);
export const POST = Symbol(`POST`);

export class Post implements IPost {
   public id = 0;
   public text = ``;
}