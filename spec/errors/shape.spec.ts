import { JTC } from '@taedr/jsontoclass';
import { CorruptionError, ERRORS } from '@taedr/requesters/core';
import { FN_COPY, FN_DELAY, FN_GET_ID } from '@taedr/utils';
import { User, UserItemShell, UserListShell, USER_LIST_MOCK } from '../_model';




describe(`Error Shape`, () => {
   it(`Corruption `, async () => {

      const result = JTC.convert(User, USER_LIST_MOCK[8], { getId: FN_GET_ID });

      const shell = new UserListShell();

      const Eerror = new CorruptionError(result);

      try {
         await shell.load({ page: 8 }).value;
      } catch(e) {
         expect(FN_COPY(e)).toEqual(FN_COPY(Eerror));
         expect(shell.data.value).toEqual([]);
         expect(shell.state.value).toEqual(`ERROR`);
      }

      await FN_DELAY(10);
   });


   it(`Object `, async () => {
      const json = []

      const shell = new UserItemShell(async () => {
         return json as any;
      });

      const Eerror = ERRORS.object(json);

      try {
         await shell.load().value;
      } catch(e) {
         expect(e).toEqual(Eerror);
      }

      await FN_DELAY(10);
   });


   it(`Array `, async () => {
      const json = {}

      const shell = new UserListShell();

      const Eerror = ERRORS.array(json);

      try {
         await shell.load({ page: 7 }).value;
      } catch(e) {
         expect(e).toEqual(Eerror);
      }
   });


   it(`Null `, async () => {
      const shell = new UserItemShell(async ({ id }) => {
         if (id === 1) return null as any;
         if (id === 2) return undefined as any;
      });

      const Eerror = ERRORS.null;

      try {
         await shell.load({ id: 1 }).value;
      } catch(e) {
         expect(e).toEqual(Eerror);
      }

      try {
         await shell.load({ id: 6 }).value;
      } catch(e) {
         expect(e).toEqual(Eerror);
      }
   });
})
