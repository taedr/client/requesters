import { ERRORS } from '@taedr/requesters/core';
import { FN_DELAY } from '@taedr/utils';
import { IUser, User, UserListShell } from '../_model';


it(`Error Fetch`, async () => {
   const shell = new UserListShell();

   expect(() => {
      shell.merge().value;
   }).toThrowError(ERRORS.merge(`INITIAL`))

   await shell.load({ page: 5 }).value;

   expect(() => {
      shell.merge().value;
   }).toThrowError(ERRORS.merge(`EMPTY`))

   await shell.load({ page: 4 }).value;

   expect(() => {
      shell.merge().value;
   }).toThrowError(ERRORS.merge(`FULL`))

   shell.load({ page: 3 }).value;

   expect(() => {
      shell.load().value;
   }).toThrowError(ERRORS.load(`LOADING`));

   expect(() => {
      shell.merge().value;
   }).toThrowError(ERRORS.merge(`LOADING`))

   await FN_DELAY(10);

   shell.merge().value;

   expect(() => {
      shell.merge().value;
   }).toThrowError(ERRORS.merge(`MERGING`));

   expect(() => {
      shell.load().value;
   }).toThrowError(ERRORS.load(`MERGING`));

   await FN_DELAY(10);
});