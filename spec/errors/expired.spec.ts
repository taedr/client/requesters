import { ExpiredError } from '@taedr/requesters/core';
import { UserItemShell, USERS_TTL_MS } from '../_model';




it(`Error Expired`, async () => {
   const shell = new UserItemShell(async () => {
      return null;
   }, 110);

   const Eerror = new ExpiredError(USERS_TTL_MS);

   try {
      await shell.load().value
   } catch (e) {
      expect(e).toEqual(Eerror);
   }

   expect(shell.data.value).toBeNull();
   expect(shell.state.value).toEqual(`ERROR`);
   expect(shell.state.error.value).toEqual(Eerror);
});