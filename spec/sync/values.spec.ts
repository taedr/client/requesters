import { UserListShell } from '../_model';
import { Address } from '../_model/address/model/address.model';
import { POST_LIST_MOCK } from '../_model/post/list/post.list.mock';



it(`Sync values`, async () => {
   const users_1 = new UserListShell();
   const users_2 = new UserListShell();
   const users_3 = new UserListShell();

   const Eposts = POST_LIST_MOCK[1];
   const EmergedPosts = [...POST_LIST_MOCK[1], ...POST_LIST_MOCK[2]];

   const Eaddress = new Address();


   await users_1.load().value;

   const first_1 = users_1.data.get(0);

   const posts = await first_1.posts.load().value;
   const adress = await first_1.address.load().value;

   expect(posts).toEqual(Eposts);
   expect(adress).toEqual(Eaddress);

   await users_2.load().value;

   const first_2 = users_2.data.get(0);

   expect(first_2.posts.data.value).toEqual(Eposts);
   expect(first_2.address.data.value).toEqual(Eaddress);

   await first_2.posts.merge().value;

   await users_3.load().value;

   const first_3 = users_3.data.get(0);

   expect(first_3.posts.data.value).toEqual(EmergedPosts);
   expect(first_3.address.data.value).toEqual(Eaddress);

   for (let i = 0; i < users_1.data.size; i++) {
      const entry_1 = users_1.data.value[i];
      const entry_2 = users_2.data.value[i];
      const entry_3 = users_3.data.value[i];

      expect(entry_1).toBe(entry_2);
      expect(entry_2).toBe(entry_3);

      expect(entry_1.posts).toBe(entry_2.posts);
      expect(entry_2.posts).toBe(entry_3.posts);

      expect(entry_1.posts).toBe(entry_2.posts);
      expect(entry_2.posts).toBe(entry_3.posts);

      expect(entry_1.address).toBe(entry_2.address);
      expect(entry_2.address).toBe(entry_3.address);
   }
});