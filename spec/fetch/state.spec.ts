import { RequesterError, ERRORS, TShellState } from '@taedr/requesters/core';
import { ARRAY, FN_DELAY, Hub, Promised } from '@taedr/utils';
import { UserListShell, User, USERS_TTL_MS } from '../_model';


describe(`State`, () => {
   const hub = new Hub();

   it(`Initial`, async () => {
      const shell = new UserListShell();
      const isInitial = shell.state.is(`INITIAL`);

      expect(isInitial).toBeTruthy();
   });


   it(`Loading`, async () => {
      const shell = new UserListShell();

      const states = shell.state.node(hub, ARRAY);
      const Estates: TShellState[] = [`INITIAL`, `LOADING`, `READY`];

      const value = shell.load().value;
      await FN_DELAY(7);

      const isLoading = shell.state.is(`LOADING`);
      const isActive = shell.state.is(`ACTIVE`);

      await value;

      const isReady = shell.state.is(`READY`);
      const isIdle = shell.state.is(`IDLE`);

      expect(isLoading).toBeTruthy();
      expect(isActive).toBeTruthy();
      expect(isReady).toBeTruthy();
      expect(isIdle).toBeTruthy();
      expect(states.value).toEqual(Estates);
   });


   it(`Error`, async () => {
      const message = `Error`;
      const shell = new UserListShell();

      const value = shell.load({ page: 10 }).value;
      await FN_DELAY(7);

      const isLoading = shell.state.is(`LOADING`);
      const isActive = shell.state.is(`ACTIVE`);

      try {
         await value;
      } catch (e) {
         expect(e).toBeInstanceOf(RequesterError);
         expect(e.message).toBe(message);
         expect(e).toBe(shell.state.error.value);
      }

      const isReady = shell.state.is(`ERROR`);
      const isIdle = shell.state.is(`IDLE`);
      const isMergable = shell.state.is(`MERGABLE`);

      expect(isLoading).toBeTruthy();
      expect(isActive).toBeTruthy();
      expect(isReady).toBeTruthy();
      expect(isIdle).toBeTruthy();
      expect(isMergable).toBeFalsy();
   });


   it(`Expired`, async () => {
      const error = ERRORS.expired(USERS_TTL_MS);
      const shell = new UserListShell();

      try {
         await shell.load({ page: 9 }).value;
      } catch (e) {
         expect(e).toBeInstanceOf(RequesterError);
         expect(e).toEqual(error);
         expect(e).toBe(shell.state.error.value);
      }
   });


   it(`Empty`, async () => {
      const shell = new UserListShell();

      const value = shell.load({ page: 5 }).value;
      await FN_DELAY(7);

      const isLoading = shell.state.is(`LOADING`);
      await value;

      const isEmpty = shell.state.is(`EMPTY`);
      const isIdle = shell.state.is(`IDLE`);
      const isMergable = shell.state.is(`MERGABLE`);

      expect(isLoading).toBeTruthy();
      expect(isEmpty).toBeTruthy();
      expect(isIdle).toBeTruthy();
      expect(isMergable).toBeFalsy();
   });


   it(`Clear`, async () => {
      const shell = new UserListShell();

      const value = shell.load().value;

      const isLoading = shell.state.is(`LOADING`);

      await value;

      const isReady = shell.state.is(`READY`);

      shell.clear();

      const isInitial = shell.state.is(`INITIAL`);
      const isIdle = shell.state.is(`IDLE`);

      expect(isLoading).toBeTruthy();
      expect(isReady).toBeTruthy();
      expect(isInitial).toBeTruthy();
      expect(isIdle).toBeTruthy();
   });


   it(`Merging`, async () => {
      const shell = new UserListShell();

      const states = shell.state.node(hub, ARRAY);
      const Estates: TShellState[] = [
         `INITIAL`, `LOADING`, `READY`, `MERGING`, `READY`, `MERGING`, `FULL`
      ];

      const first = shell.load({ page: 1 }).value;

      await FN_DELAY(5);

      const isMergable_1 = shell.state.is(`MERGABLE`);
      const isLoading = shell.state.is(`LOADING`);

      await first;

      const isReady = shell.state.is(`READY`);
      const isMergable_2 = shell.state.is(`MERGABLE`);

      const second = shell.merge({ page: 2 }).value;

      await FN_DELAY(5);

      const isMerging = shell.state.is(`MERGING`);
      const isActive = shell.state.is(`ACTIVE`);

      await second;

      const isMerged = shell.state.is(`READY`);

      await shell.merge({ page: 4 }).value;

      const isFull = shell.state.is(`FULL`);
      const isMergable_3 = shell.state.is(`MERGABLE`);


      expect(isMergable_1).toBeFalsy();
      expect(isMergable_2).toBeTruthy();
      expect(isLoading).toBeTruthy();
      expect(isReady).toBeTruthy();
      expect(isMerging).toBeTruthy();
      expect(isActive).toBeTruthy();
      expect(isMerged).toBeTruthy();
      expect(isFull).toBeTruthy();
      expect(isMergable_3).toBeFalsy();
      expect(states.value).toEqual(Estates);
   });
});