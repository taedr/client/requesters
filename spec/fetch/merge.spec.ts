import { HEAD } from '@taedr/arrays';
import { FN_COPY } from '@taedr/utils';
import { IUser, User, UserListShell, USER_LIST_MOCK } from '../_model';



it(`Merge`, async () => {
   const shell = new UserListShell();

   const Emerge_1 = [...USER_LIST_MOCK[1], ...USER_LIST_MOCK[2]];
   const Emerge_2 = [...USER_LIST_MOCK[3], ...Emerge_1];
   const Emerge_3 = [...Emerge_2];

   Emerge_3.splice(3, 0, ...USER_LIST_MOCK[4]);

   const users_1 = await shell.load().value;

   expect(FN_COPY(users_1)).toEqual(USER_LIST_MOCK[1]);
   expect(users_1).toEqual(shell.data.value);

   for (const user of users_1) {
      expect(user).toBeInstanceOf(User);
   }

   const request_2 = shell.merge({ page: 2 })
   const users_2 = await request_2.value;
   const users_pack_2 = await request_2.pack;

   expect(FN_COPY(users_2)).toEqual(Emerge_1);
   expect(users_2).toEqual(shell.data.value);
   expect(FN_COPY(users_pack_2)).toEqual(USER_LIST_MOCK[2]);

   const request_3 = shell.merge({ page: 3 }, HEAD)
   const users_3 = await request_3.value;
   const users_pack_3 = await request_3.pack;

   expect(FN_COPY(users_3)).toEqual(Emerge_2);
   expect(users_3).toEqual(shell.data.value);
   expect(FN_COPY(users_pack_3)).toEqual(USER_LIST_MOCK[3]);

   const request_4 = shell.merge({ page: 4 }, 3)
   const users_4 = await request_4.value;
   const users_pack_4 = await request_4.pack;

   expect(FN_COPY(users_4)).toEqual(Emerge_3);
   expect(users_4).toEqual(shell.data.value);
   expect(FN_COPY(users_pack_4)).toEqual(USER_LIST_MOCK[4]);
});