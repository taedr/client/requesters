import { ERRORS } from '@taedr/requesters/core';
import { NullShell } from '../_model';


describe(`Null`, () => {
   it(`Nullable`, async () => {
      const shell = new NullShell();
   
      const users_1 = await shell.load().value;
   
      expect(users_1).toBeNull();
      expect(users_1).toEqual(shell.data.value);
   });


   it(`Something `, async () => {
      const json = [];

      const shell = new NullShell(async () => json);

      const Eerror = ERRORS.shouldNull(json);

      try {
         await shell.load().value;
      } catch(e) {
         expect(e).toEqual(Eerror);
      }
   });
})

