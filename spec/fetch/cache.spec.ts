import { AShell } from '@taedr/requesters/core';
import { FN_COPY } from '@taedr/utils';
import { IUser, UserListShell, UserItemShell, USER_LIST_MOCK } from '../_model';


describe(`Cache`, () => {
   it(`List`, async () => {
      const list = new UserListShell();

      const users_1 = await list.load({ page: 1 }).value;
      const users_2 = await list.load({ page: 2 }).value;
      const users_3 = await list.load({ page: 3 }).value;

      
      const cache = list['_ctx'].cache //?

      expect(cache['_hashMap'].size).toBe(3);

      // AShell


      // expect(list.state.value).toEqual(`INITIAL`);
   });


   
});
