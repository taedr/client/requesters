import { FN_COPY } from '@taedr/utils';
import { IUser, UserListShell, UserItemShell, USER_LIST_MOCK } from '../_model';


describe(`Clear`, () => {
   it(`List`, async () => {
      const list = new UserListShell();

      expect(list.data.value).toEqual([]);
      expect(list.state.value).toEqual(`INITIAL`);

      const users = await list.load().value;

      expect(FN_COPY(users)).toEqual(USER_LIST_MOCK[1]);
      expect(users).toEqual(list.data.value);
      expect(list.state.value).toEqual(`READY`);

      list.clear();

      expect(list.data.value).toEqual([]);
      expect(list.state.value).toEqual(`INITIAL`);
   });


   it(`Item`, async () => {
      const json: IUser = { id: 1, name: `Vasek` };

      const item = new UserItemShell(async () => json);

      expect(item.data.value).toBeNull();
      expect(item.state.value).toEqual(`INITIAL`);

      const user = await item.load().value;

      expect(FN_COPY(user)).toEqual(json);
      expect(user).toEqual(item.data.value);
      expect(item.state.value).toEqual(`READY`);

      item.clear();

      expect(item.data.value).toBeNull();
      expect(item.state.value).toEqual(`INITIAL`);
   });
});
