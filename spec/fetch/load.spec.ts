import { ARRAY, FN_COPY, Hub } from '@taedr/utils';
import { IUser, User, UserListShell, USER_LIST_MOCK } from '../_model';


it(`Load`, async () => {
   const shell = new UserListShell();

   const hub = new Hub();
   const values = shell.data.node(hub, ARRAY);
   const Evalues = [[], USER_LIST_MOCK[1], [], USER_LIST_MOCK[2]];

   const users_1 = await shell.load({ page: 1 }).value;

   expect(FN_COPY(users_1)).toEqual(USER_LIST_MOCK[1]);
   expect(users_1).toEqual(shell.data.value);

   for (const user of users_1) {
      expect(user).toBeInstanceOf(User);
   }

   const users_2 = await shell.load({ page: 2 }).value;

   expect(FN_COPY(users_2)).toEqual(USER_LIST_MOCK[2]);
   expect(users_2).toEqual(shell.data.value);
   expect(FN_COPY(values.value)).toEqual(Evalues);

   for (const user of users_2) {
      expect(user).toBeInstanceOf(User);
   }
});