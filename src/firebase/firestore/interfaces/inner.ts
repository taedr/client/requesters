import { JTC } from '@taedr/jsontoclass';
import * as firebase from 'firebase/app';
import 'firebase/firestore';


firebase.firestore.Timestamp[JTC.FILL] = () => new firebase.firestore.Timestamp(0, 0);
firebase.firestore.GeoPoint[JTC.FILL] = () => new firebase.firestore.GeoPoint(0, 0);