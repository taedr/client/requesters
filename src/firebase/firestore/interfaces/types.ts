import { IdStr, TDeepPartial } from '@taedr/utils';

export type TCollectionType = `GLOBAL` | `SHARD`;
export type TFirestoreDoc = firebase.firestore.DocumentData;


export type TFirestore = firebase.firestore.Firestore;
export type TFirestoreQuery = firebase.firestore.Query;
export type TFirestoreQuerySnap = firebase.firestore.QuerySnapshot<firebase.firestore.DocumentData>;

export type TFirestoreDocQuery = firebase.firestore.DocumentReference<firebase.firestore.DocumentData>;
export type TFirestoreDocSnap = firebase.firestore.DocumentSnapshot<firebase.firestore.DocumentData>;
export type TPartialId<T extends object> = (TDeepPartial<T> & IdStr);
