import { IdStr, TProducer, TReadonlyArray } from '@taedr/utils';
import { AShellMock, IArrayLoad, IIdObjectLoad, IObjectLoad, IPerform, IRequest, IRequester, TModels } from '../../../core';
import { ArrayRequest, IArrayRequest, IObjectRequest, ObjectRequest } from '../../../core/utils/request';
import { TFirestore } from './types';


export interface ICollectionLoad extends IArrayLoad<ICollectionFilter> {
}


export interface ICollectionRequest extends IRequest {
   readonly config: ICollectionLoad;
}


export interface IDocumentLoad extends IIdObjectLoad<null> {
   id: string;
}


export interface IDocumentRequest extends IRequest {
   readonly config: IDocumentLoad;
}

export interface ICollectionFilter {
   orderBy?: { field: string, direction: 'asc' | 'desc' }
}


export interface IFirestoreActionsCtx<M extends object> {
   models: TModels<M>;
   mock: TProducer<AShellMock>;
   firestore: TFirestore;
   ttlSec: number;
   isDebug?: boolean;
}