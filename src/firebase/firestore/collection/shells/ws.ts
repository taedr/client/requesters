import { IChange } from '@taedr/arrays';
import { FN_GET_ID_STR, IdStr } from '@taedr/utils';
import { ISyncEvent } from '../../../../core';
import { ICollectionLoad } from '../../interfaces/filter';
import { TCollectionType, TFirestore } from '../../interfaces/types';
import { FirestoreCollectionWs } from '../requesters/ws';
import { ACollectionShell } from './core';
import { ICollectionHttpCtx } from './http';

export interface ICollectionLoadWs extends Omit<ICollectionLoad, 'isCheckSync'> {
}


export interface ICollectionWsCtx<I extends ICollectionLoadWs, O extends IdStr>
   extends Omit<ICollectionHttpCtx<I, O>, 'actions' | 'cache'> {
}


export class CollectionWs<I extends ICollectionLoadWs, O extends IdStr> extends ACollectionShell<I, O> {
   constructor(
      ctx: ICollectionWsCtx<I, O>,
      firestore: TFirestore,
      type: TCollectionType,
   ) {
      const requester = new FirestoreCollectionWs(firestore, type);

      super({
         ...ctx,
         requester,
      });
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------

   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
}