import { IdStr } from '@taedr/utils';
import { ICollectionLoad } from '../../interfaces/filter';
import { TCollectionType, TFirestore } from '../../interfaces/types';
import { FirestoreCollectionHttp } from '../requesters/http';
import { ACollectionShell, ICollectionShellCtx } from './core';

export interface ICollectionHttpCtx<I extends ICollectionLoad, O extends IdStr>
   extends Omit<ICollectionShellCtx<I, O>, 'requester'> {
}


export class CollectionHttp<I extends ICollectionLoad, O extends IdStr> extends ACollectionShell<I, O> {
   constructor(
      ctx: ICollectionHttpCtx<I, O>,
      firestore: TFirestore,
      type: TCollectionType,
   ) {
      const requester = new FirestoreCollectionHttp(firestore, type);

      super({
         ...ctx,
         requester,
      });
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
}