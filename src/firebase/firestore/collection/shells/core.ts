import { IdStr, TReadonlyArray } from '@taedr/utils';
import { ArrayShell, IArrayShellCtx, IRequester } from '../../../../core';
import { IObjectRequest } from '../../../../core/utils/request';
import { ICollectionLoad } from '../../interfaces/filter';

export interface ICollectionShellCtx<T extends IdStr> extends IArrayShellCtx<T> {
   requester: IRequester;
}


export abstract class ACollectionShell<T extends IdStr> extends ArrayShell<T> {
   constructor(
      protected _ctx: ICollectionShellCtx<T>
   ) {
      super(_ctx);
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public load(config: ICollectionLoad): IObjectRequest<TReadonlyArray<T>> {
      return super.load(config);
   }


   public merge() {
      return super.merge({ ...this._request.config });
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
}


