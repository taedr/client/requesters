import { JTC } from '@taedr/jsontoclass';
import { IdStr, TReadonlyArray } from '@taedr/utils';
import { firestore } from 'firebase/app';
import { IRequester, IObjectLoad, IRequest, IPerform, ActionsShell, ArrayActionShell, SyncActionsShell } from '../../../../../core';
import { IFirestoreActionsCtx } from '../../../interfaces/filter';
import { TFirestoreDocQuery } from '../../../interfaces/types';


export class FirestoreAdd<T extends IdStr> implements IRequester<IObjectLoad<T[]>> {
   constructor(
      protected _firestore: firebase.firestore.Firestore,
   ) {
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public async load(req: IRequest<IPerform<T[]>>) {
      const payload = req.config.payload ?? [];
      if (payload.length === 0) return; // #Return#

      const url = req.getPathAsString();
      const ref = this._firestore.collection(url);
      const promises: Promise<TFirestoreDocQuery>[] = [];

      for (const value of payload) {
         const copy = this.getCopy(value);
         const promise = ref.add(copy);
         promises.push(promise);
      }


      try {
         const docs = await Promise.all(promises);
         const data = docs.map((doc, i) => ({
            id: doc.id,
            ...payload[i],
         })) as T[];

         req.resolve(data);
      } catch (e) {
         req.reject(e);
      }
   }

   public abort() { }

   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected getCopy<T extends object>(data: T): object {
      if (data['constructor'] === Object) {
         delete data['id'];
         return data;
      }

      const copy = JTC.deconvert(data, {
         leaveAsIs: [firestore.Timestamp, firestore.GeoPoint]
      });

      delete copy['id'];
      return copy;
   }
}





export class FirestoreAddShell<I extends IdStr, O extends I> extends SyncActionsShell<IPerform<I[]>, TReadonlyArray<O>, O> {
   constructor({
      models,
      mock,
      ttlSec,
      isDebug,
      firestore
   }: IFirestoreActionsCtx<I, O>) {
      super({
         type: `ADD`,
         getPerformer: index => new ArrayActionShell({
            id: `${name} | Add | ${index}`,
            requester: new FirestoreAdd(firestore),
            mock: mock(),
            models,
            ttlSec,
            isDebug,
         }),
      })
   }
}