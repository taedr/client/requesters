import { sliceOnPacks } from '@taedr/arrays';
import { JTC } from '@taedr/jsontoclass';
import { FN_MIRROR, IdStr, TReadonlyArray } from '@taedr/utils';
import { firestore } from 'firebase/app';
import { IRequester, IObjectLoad, IRequest, IPerform, ActionsShell, ArrayActionShell, SyncActionsShell } from '../../../../../core';
import { IFirestoreActionsCtx } from '../../../interfaces/filter';
import { TPartialId } from '../../../interfaces/types';




export class FirestoreUpdate<T extends IdStr> implements IRequester<IObjectLoad<TPartialId<T>[]>> {
   constructor(
      protected _firestore: firebase.firestore.Firestore,
   ) {
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public async load(req: IRequest<IPerform<TPartialId<T>[]>>) {
      const payload = req.config.payload;
      if (payload.length === 0) return; // #Return#

      const url = req.getPathAsString();
      const ref = this._firestore.collection(url);
      const promises: Promise<any>[] = [];
      const packs = sliceOnPacks(payload, 500);

      for (const pack of packs) {
         const batch = this._firestore.batch();

         for (const value of pack) {
            const copy = this.getCopy(value);
            const doc = ref.doc(value.id);
            batch.update(doc, copy);
         }

         promises.push(batch.commit());
      }

      try {
         await Promise.all(promises);
         req.resolve(payload);
      } catch (e) {
         req.reject(e);
      }
   }


   public abort() {}
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected getCopy<T extends object>(data: T): object {
      if (data['constructor'] === Object) return data;

      const copy = JTC.deconvert(data, {
         leaveAsIs: [firestore.Timestamp, firestore.GeoPoint]
      });

      delete copy['id'];
      return copy;
   }

}


export class FirestoreUpdateShell<I extends IdStr, O extends I> extends SyncActionsShell<IPerform<TPartialId<I>[]>, TReadonlyArray<O>, O> {
   constructor({
      models,
      mock,
      ttlSec,
      isDebug,
      firestore
   }: IFirestoreActionsCtx<TPartialId<I>, O>) {
      super({
         type: `UPDATE`,
         getPerformer: index => new ArrayActionShell({
            id: `${name} | Update | ${index}`,
            requester: new FirestoreUpdate(firestore),
            SStructure: `PARTIAL`,
            mock: mock(),
            models,
            ttlSec,
            isDebug,
         }),
      })
   }
}