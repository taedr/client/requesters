import { FN_MIRROR, IdStr, TReadonlyArray } from '@taedr/utils';
import { IRequester, IObjectLoad, IRequest, IPerform, ActionsShell, ArrayActionShell, SyncActionsShell } from '../../../../../core';
import { IFirestoreActionsCtx } from '../../../interfaces/filter';



export class FirestoreDelete<T extends IdStr> implements IRequester<IObjectLoad<T[]>> {
   constructor(
      protected _firestore: firebase.firestore.Firestore,
   ) {
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public async load(req: IRequest<IPerform<T[]>>) {
      const payload = req.config.payload;
      if (payload.length === 0) return; // #Return#

      const url = req.getPathAsString();
      const batch = this._firestore.batch();
      const ref = this._firestore.collection(url);

      for (const value of payload) {
         if (!value.id) continue;
         batch.delete(ref.doc(value.id));
      }

      try {
         await batch.commit();
         req.resolve(payload);
      } catch (e) {
         req.reject(e);
      }
   }

   
   public abort() {}
}



export class FirestoreDeleteShell<I extends IdStr, O extends I> extends SyncActionsShell<IPerform<I[]>, TReadonlyArray<O>, O> {
   constructor({
      models,
      mock,
      ttlSec,
      isDebug,
      firestore
   }: IFirestoreActionsCtx<I, O>) {
      super({
         type: `DELETE`,
         getPerformer: index => new ArrayActionShell({
            id: `${name} | Delete | ${index}`,
            requester: new FirestoreDelete(firestore),
            mock: mock(),
            models,
            ttlSec,
            isDebug,
         }),
      })
   }
}