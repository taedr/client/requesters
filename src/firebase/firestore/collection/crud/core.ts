import * as firebase from 'firebase/app';
import { IdStr, TDeepPartial, TNew, TProducer, TReadonlyArray } from '@taedr/utils';
import { ActionsShell, ArrayActionShell, AShellMock, IObjectLoad, IPath, IPerform, TModels } from '../../../../core';
import { TPartialId } from '../../interfaces/types';
import { FirestoreAdd, FirestoreAddShell } from './actions/add';
import { FirestoreDelete, FirestoreDeleteShell } from './actions/delete';
import { FirestoreUpdate, FirestoreUpdateShell } from './actions/update';


export interface ICrudEntry<T extends IdStr> {
   ttlSec: number;
   mock: TProducer<AShellMock<IPerform<T[]>>>;
}


export interface IFiresyoreCrudCtx<I extends IdStr, O extends I> {
   models: TModels<O>,
   isDebug: boolean,
   firestore: firebase.firestore.Firestore,
   add: ICrudEntry<I>;
   update: ICrudEntry<TPartialId<I>>;
   delete: ICrudEntry<I>;
}


export class FirestoreCrud<I extends IdStr, O extends I>  {
   readonly add: FirestoreAddShell<I, O>;
   readonly delete: FirestoreDeleteShell<I, O>;
   readonly update: FirestoreUpdateShell<I, O>;


   constructor({
      models,
      isDebug,
      add,
      update,
      delete: remove,
      firestore,
   }: IFiresyoreCrudCtx<I, O>) {
      this.add = new FirestoreAddShell({
         models,
         firestore,
         isDebug,
         ttlSec: add.ttlSec,
         mock: add.mock,
      });
      
      this.delete = new FirestoreDeleteShell({
         models,
         firestore,
         isDebug,
         ttlSec: remove.ttlSec,
         mock: remove.mock,
      });

      this.update = new FirestoreUpdateShell({
         models,
         firestore,
         isDebug,
         ttlSec: update.ttlSec,
         mock: update.mock,
      });
   }


   public lock(path: IPath) {
      return new FirestoreCrudLock(path, this);
   }
}

export class FirestoreCrudLock<I extends IdStr, O extends I> {
   constructor(
      protected _path: IPath,
      protected _crud: FirestoreCrud<I, O>
   ) {

   }


   public add(payload: I[]) {
      this._crud.add.perform({
         path: this._path,
         payload
      });
   }


   public delete(payload: I[]) {
      this._crud.delete.perform({
         path: this._path,
         payload
      });
   }


   public update(payload: (TDeepPartial<I> & IdStr)[]) {
      this._crud.update.perform({
         path: this._path,
         payload
      });
   }
}