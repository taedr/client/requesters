import { IChange } from '@taedr/arrays';
import { Bus } from '@taedr/reactive';
import { FN_NULL, IdStr, TFunction, } from '@taedr/utils';
import { IChanges } from '../../../../core';
import { ICollectionRequest } from '../../interfaces/filter';
import { TCollectionType, TFirestore, TFirestoreQuery, TFirestoreQuerySnap } from '../../interfaces/types';
import { AFirestoreCollectionCore } from './core';


export class FirestoreCollectionWs extends AFirestoreCollectionCore {
   protected _updates = 0;
   protected _currLen = 0;
   protected _detacher: TFunction = FN_NULL;
   protected _changes = new Bus<IChanges>();

   public get changes() { return this._changes; }

   constructor(
      firestore: TFirestore,
      type: TCollectionType,
   ) {
      super(firestore, type);
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public abort(): void {
      this._detacher();
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected async setQuery(query: TFirestoreQuery, req: ICollectionRequest, isPaginate: boolean) {
      let _limit = req.config.packSize;
      if (isPaginate) {
         this._currLen = (req.curr as any[]).length;
         _limit += this._currLen;
      } else {
         this._currLen = 0;
      }

      query = query.limit(_limit);

      this._updates = 0;
      this.abort();
      this._detacher = query.onSnapshot({
         next: snap => this.onChange(snap, req),
         error: e => req.reject(e),
      });
   }


   protected onChange(snap: TFirestoreQuerySnap, req: ICollectionRequest) {
      const changes = snap.docChanges();

      if (this._updates++ === 0) {
         const pack = changes.slice(this._currLen);
         const data = pack.map(change => ({
            id: change.doc.id,
            ...change.doc.data()
         }));

         req.resolve(data);
      } else {
         this.sendEvents(changes);
      }
   }


   protected sendEvents(changes: firebase.firestore.DocumentChange<firebase.firestore.DocumentData>[]) {
      const added: IChange<IdStr>[] = [];
      const deleted: IdStr[] = [];
      const updated: IdStr[] = [];

      for (const { doc, type, newIndex } of changes) {
         const value = { id: doc.id, ...doc.data() };
         const index = newIndex;

         switch (type) {
            case `added`: added.push({ index, value, }); break;
            case `modified`: updated.push(value); break;
            case `removed`: deleted.push(value); break;
         }
      }

      this._changes.value = { added, updated, deleted };
   }
}