import { IRequester } from '../../../../core';
import { MergeRequest } from '../../../../core/utils/request';
import { ICollectionLoad, ICollectionRequest } from '../../interfaces/filter';
import { TCollectionType, TFirestore, TFirestoreQuery } from '../../interfaces/types';




export abstract class AFirestoreCollectionCore implements IRequester {

   constructor(
      protected _firestore: TFirestore,
      protected _type: TCollectionType,
   ) {
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   protected abstract setQuery(query: TFirestoreQuery, request: ICollectionRequest, isPaginate: boolean): void;
   public abstract abort(request: ICollectionRequest): void;
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public async load(request: ICollectionRequest) {
      const query = this.getQuery(request);
      const isPaginate = request instanceof MergeRequest;


      this.setQuery(query, request, isPaginate);
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected getQuery(request: ICollectionRequest) {
      let query: TFirestoreQuery;
      const filter = request.config.payload;

      switch (this._type) {
         case `SHARD`: query = this.getShardQuery(request); break;
         case `GLOBAL`: query = this.getGlobalQuery(request); break;
      }

      if (filter?.orderBy) query = query.orderBy(filter.orderBy.field, filter.orderBy.direction);

      return query;
   }


   protected getShardQuery(req: ICollectionRequest): TFirestoreQuery {
      const url = req.config.path.join(`/`);
      let query: TFirestoreQuery = this._firestore.collection(url);

      return query;
   }


   protected getGlobalQuery(req: ICollectionRequest): TFirestoreQuery {
      const url = req.config.path.slice(-1)[0];
      const entries = Object.entries(req.config.path).slice(0, -1);
      let query: TFirestoreQuery = this._firestore.collectionGroup(url);

      for (const [key, value] of entries) {
         if (value === null) continue; // #Continue#
         console.log(`_relation.${key}`, `==`, value)
         query = query.where(`_relation.${key}`, `==`, value);
      }


      return query;
   }
}