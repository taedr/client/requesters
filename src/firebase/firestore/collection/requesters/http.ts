import * as firebase from 'firebase/app';
import { ICollectionRequest } from '../../interfaces/filter';
import { TCollectionType, TFirestore, TFirestoreQuery } from '../../interfaces/types';
import { AFirestoreCollectionCore } from './core';



export class FirestoreCollectionHttp extends AFirestoreCollectionCore {
   protected _last: firebase.firestore.QueryDocumentSnapshot<firebase.firestore.DocumentData>


   constructor(
      firestore: TFirestore,
      type: TCollectionType,
   ) {
      super(firestore, type);
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public abort() { }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected async setQuery(query: TFirestoreQuery, req: ICollectionRequest, isPaginate: boolean) {
      if (isPaginate) {
         const lastId = (req.curr as any[]).slice(-1)[0].id;

         if (this._last?.id !== lastId) {
            const url = [...req.config.path, lastId].join(`/`);
            this._last = await this._firestore.doc(url).get();
         }

         query = query.startAfter(this._last);
      }

      query = query.limit(req.config.packSize);

      try {
         const snap = await query.get();
         const data = snap.docs.map(doc => ({
            id: doc.id,
            ...doc.data()
         }));

         this._last = snap.docs.slice(-1)[0];

         req.resolve(data)
      } catch (e) {
         req.reject(e);
      }
   }
}