import { Bus } from '@taedr/reactive';
import { FN_NULL, IdStr, TFunction } from '@taedr/utils';
import { IChanges } from '../../../../core';
import { IDocumentRequest } from '../../interfaces/filter';
import { TFirestore, TFirestoreDocQuery, TFirestoreDocSnap } from '../../interfaces/types';
import { AFirestoreDocumentCore } from './core';



export class FirestoreDocumentWs extends AFirestoreDocumentCore {
   protected _updates = 0;
   protected _detacher: TFunction = FN_NULL;
   protected _changes = new Bus<IChanges>();

   public get changes() { return this._changes;  }

   constructor(
      firestore: TFirestore,
   ) {
      super(firestore);
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public abort(): void {
      this._detacher();
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected async setQuery(query: TFirestoreDocQuery, req: IDocumentRequest) {
      this._updates = 0;
      this.abort();
      this._detacher = query.onSnapshot({
         next: snap => this.onChange(snap, req),
         error: e => req.reject(e),
      });
   }


   protected onChange(snap: TFirestoreDocSnap, req: IDocumentRequest) {
      const value: any = { id: snap.id, ...snap.data() };
      const updated: IdStr[] = [];
      const deleted: IdStr[] = [];

      if (this._updates++ === 0) {
         req.resolve(value);
      } else {
         switch (snap.exists) {
            case true: updated.push(value); break;
            case false: deleted.push(value); break;
         }
      }

      this._changes.value = { added: [], updated, deleted };
   }
}