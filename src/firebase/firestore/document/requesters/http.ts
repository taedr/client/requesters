import { RequesterError } from '../../../../core';
import { IDocumentRequest } from '../../interfaces/filter';
import { TFirestore, TFirestoreDocQuery } from '../../interfaces/types';
import { AFirestoreDocumentCore } from './core';



export class FirestoreDocumentHttp extends AFirestoreDocumentCore {
   constructor(
      firestore: TFirestore,
   ) {
      super(firestore);
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public abort(): void { }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected async setQuery(query: TFirestoreDocQuery, req: IDocumentRequest) {
      try {
         const snap = await query.get();
         const data = { id: snap.id, ...snap.data() };
         
         if (snap.exists) {
            req.resolve(data);
         } else {
            req.reject(new RequesterError(404, `Document doesn't exist`));
         }
      } catch (e) {
         req.reject(e);
      }
   }
}