import { IRequester, IIdObjectLoad, IRequest } from '../../../../core';
import { IDocumentLoad, IDocumentRequest } from '../../interfaces/filter';
import { TFirestore, TFirestoreDocQuery } from '../../interfaces/types';


export abstract class AFirestoreDocumentCore implements IRequester {

   constructor(
      protected _firestore: TFirestore,
   ) {
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   protected abstract setQuery(query: TFirestoreDocQuery, req: IDocumentRequest): void;
   public abstract abort(request: IDocumentRequest): void;
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public async load(req: IDocumentRequest) {
      const url = [...req.config.path, req.config.id].join(`/`);
      const query = this._firestore.doc(url);
      this.setQuery(query, req);
   }
}