import { IdStr } from '@taedr/utils';
import { TFirestore } from '../../interfaces/types';
import { FirestoreDocumentHttp } from '../requesters/http';
import { ADocumentShell, IDocumentShellCtx } from './core';

export interface IDocumentHttpCtx<T extends IdStr>
   extends Omit<IDocumentShellCtx<T>, 'requester'> {
}


export class DocumentHttp<T extends IdStr> extends ADocumentShell<T> {
   constructor(
      ctx: IDocumentHttpCtx<T>,
      firestore: TFirestore,
   ) {
      const requester = new FirestoreDocumentHttp(firestore);

      super({
         ...ctx,
         requester,
      });
   }
}