import { IdStr } from '@taedr/utils';
import { IObjectRequest } from '../../../../core/utils/request';
import { IDocumentLoad } from '../../interfaces/filter';
import { TFirestore } from '../../interfaces/types';
import { FirestoreDocumentWs } from '../requesters/ws';
import { ADocumentShell } from './core';
import { IDocumentHttpCtx } from './http';

export interface IDocumentLoadWs extends Omit<IDocumentLoad, 'isCheckSync'> {
}


export interface IDocumentWsCtx<T extends IdStr>
   extends Omit<IDocumentHttpCtx<T>, 'actions' | 'cache'> {
}


export class DocumentWs<T extends IdStr> extends ADocumentShell<T> {
   constructor(
      ctx: IDocumentWsCtx<T>,
      firestore: TFirestore,
   ) {
      const requester = new FirestoreDocumentWs(firestore);

      super({
         ...ctx,
         requester,
      });
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public load(config: IDocumentLoadWs): IObjectRequest<T> {
      return super.load(config);
   }
}