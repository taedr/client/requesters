import { IdStr } from '@taedr/utils';
import { IdObjectShell, IIdObjectShellCtx } from '../../../../core';
import { IObjectRequest } from '../../../../core/utils/request';
import { IDocumentLoad } from '../../interfaces/filter';

export interface IDocumentShellCtx<T extends IdStr> extends IIdObjectShellCtx<T> {

}


export abstract class ADocumentShell<T extends IdStr> extends IdObjectShell<T> {
   constructor(
      protected _ctx: IDocumentShellCtx<T>
   ) {
      super(_ctx);
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   public load(config: IDocumentLoad): IObjectRequest<T> {
      return super.load(config);
   }
}


