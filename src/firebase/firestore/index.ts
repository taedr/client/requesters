export * from './interfaces/types';
export * from './interfaces/inner';
export * from './interfaces/filter';

export * from './collection/crud';
export * from './collection/requesters/core';
export * from './collection/requesters/http';
export * from './collection/requesters/ws';
export * from './collection/shells/core';
export * from './collection/shells/http';
export * from './collection/shells/ws';

export * from './document/requesters/core';
export * from './document/requesters/http';
export * from './document/requesters/ws';
export * from './document/shells/core';
export * from './document/shells/http';
export * from './document/shells/ws';


import { JTC } from '@taedr/jsontoclass';
import * as firebase from 'firebase/app';
import  'firebase/firestore';

// firebase.firestore.Timestamp[JTC.FILL] = () => new firebase.firestore.Timestamp(0, 0);
// firebase.firestore.GeoPoint[JTC.FILL] = () => new firebase.firestore.GeoPoint(0, 0);

