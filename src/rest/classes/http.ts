import { ERRORS, IRequest, IObjectLoad, IRequester } from '../../core';
import { TRequestMethod, IS } from '@taedr/utils';


// ==================================================
//                   Model
// ==================================================
export interface IHttpRequesterCtx {
   readonly domain: string
   readonly method: TRequestMethod;
}
// ==================================================
//                   Sender
// ==================================================
export class HttpRequester implements IRequester<IObjectLoad<object>> {
   protected _aborter: AbortController;

   constructor(
      readonly _ctx: IHttpRequesterCtx,
   ) {
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public async load(req: IRequest<IObjectLoad<object>>) {
      if (!navigator.onLine) { // #Error#
         return req.reject(ERRORS.noInternet);
      }

      this.abort();

      try {
         const url = req.getPathAsString();
         const payload = req.config.payload;
         const data = await this.fetch(url, payload);
         req.resolve(data);
      } catch (e) {
         req.reject(e);
      }
   }


   public abort() {
      this._aborter?.abort();
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected async fetch(path: string, payload: object): Promise<any> {
      const aborter = this._aborter = new AbortController();
      const method = this._ctx.method;

      let urlStr = [this._ctx.domain, path].join(`/`);

      const contentKey = 'Content-Type'
      const init: RequestInit = {
         method,
         signal: aborter.signal,
         mode: this.getMode(path, payload),
         headers: this.getHeaders(path, payload),
      }

      if (!IS.null(payload)) {
         if (method === `GET`) {
            urlStr += this.toUrlParams(payload);
         } else {
            if (payload instanceof FormData) {
               init.body = payload;
               init.headers[contentKey] = `multipart/form-data`;
            } else {
               init.body = JSON.stringify(payload);
               init.headers[contentKey] = `application/json`;
            }
         }
      }

      const response = await this.send(urlStr, init);
      const json = this.getJson(response);

      return json;
   }


   protected send(url: string, init: RequestInit) {
      return fetch(url, init);
   }


   protected getHeaders(path: string, payload: object): HeadersInit {
      return {};
   }


   protected getMode(path: string, payload: object): RequestMode {
      return `cors`;
   }

   protected async getJson(response: Response) {
      return response.json();
   }


   protected toUrlParams(object: object): string {
      const entries = Object.entries(object);
      if (entries.length === 0) return ``; // #Return#
   
      const params: string[] = entries.reduce((acc, [key, value]) => {
         const val = IS.primitive(value) ? value : JSON.stringify(value);
         acc.push(`${key}=${val}`);
         return acc;
      }, [])
   
      return `?` + params.join('&');
   }
}


