// import { RequesterError, ERRORS, IRequest, IObjectLoad, IRequester } from '../../core';


// // ==================================================
// //                   Model
// // ==================================================
// export interface IWsRequesterCtx {
//    readonly domain: string
// }
// // ==================================================
// //                   Core
// // ==================================================
// export class WsRequester implements IRequester<IObjectLoad<object>> {
//    protected _socket: WebSocket = null;

//    constructor(
//       protected _ctx: IWsRequesterCtx,
//    ) {
//    }
//    // ---------------------------------------------
//    //                Api
//    // ---------------------------------------------
//    public async load(req: IRequest<IObjectLoad<object>>) {
//       const isConnected = this._socket?.readyState === 1;
     
//       if (isConnected) {
//          this.send(req);
//       } else {
//          this.connect(req);
//       }
//    }

 
//    public abort() {

//    }
 

//    public clear() {
//       this._socket.close(1000, JSON.stringify({ message: `Client closed connection.` }));
//    }
//    // ---------------------------------------------
//    //                Internal
//    // ---------------------------------------------
//    protected async connect(req: IRequest<IObjectLoad<object>>) {
//       const path = req.getPathAsString();
//       const payload = req.config.payload;
//       const url = [this._ctx.domain, path].join(`/`);

//       this._socket = new WebSocket(url);
//       this._socket.onopen = () => this.send(payload);
//       this._socket.onerror = (e) => req.reject(e);
//       this._socket.onmessage = message => this.onIncome(message.data);
//       this._socket.onclose = event => this.onClose(event);
//    }


//    protected send(payload: object) {
//       const message =  JSON.stringify(payload);
//       this._socket.send(message);
//    }


//    protected onClose({ code, reason }: CloseEvent) {
//       if (code === 1000) return null; // #Return#
//       const error = code === 1006 ? ERRORS.wsClose : new RequesterError(code, reason);
//    }
// }

