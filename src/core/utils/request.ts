import { FN_NULL, getHash, IId, IS, TConsumer, TReadonlyArray } from '@taedr/utils';
import { RequesterError } from './errors';
import { IObjectLoad } from '../interfaces/shell';
import { IArrayLoad } from '../interfaces/array';
import { IIdObjectLoad } from '../interfaces/object';

// ==================================================
//                   Interface
// ==================================================
export interface IObjectRequest<T> {
   config: IObjectLoad<object>;
   value: Promise<T>;
}

export interface IArrayRequest<T extends IId> extends IObjectRequest<TReadonlyArray<T>> {
}

export interface IMergeRequest<T extends IId> extends IArrayRequest<T> {
}
// ==================================================
//                   Load
// ==================================================
export class ObjectRequest<T extends object> implements IObjectRequest<T> {
   protected _dataResolver: TConsumer<T>;
   protected _dataRejecter: TConsumer<RequesterError>;
   protected _rawResolver: TConsumer<any>;
   protected _rawRejecter: TConsumer<any>;

   readonly hash: number;
   readonly value = new Promise<T>((resolve, reject) => {
      this._dataResolver = resolve;
      this._dataRejecter = reject;
   });

   public raw = new Promise<any>((resolve, reject) => {
      this._rawResolver = resolve;
      this._rawRejecter = reject;
   });

   constructor(
      readonly config: IObjectLoad<object>,
   ) {
      this.hash = getHash({ config });
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public resolveData(data: T) {
      this._dataResolver(data);
      this.clear();
   }


   public rejectData(error: RequesterError) {
      this.value.catch(FN_NULL);
      this._dataRejecter(error);
      this.clear();
   }


   public resolve(raw: any) {
      if (this._rawResolver) this._rawResolver(raw);
   }


   public reject(error: any) {
      if (this._rawRejecter) this._rawRejecter(error);
   }


   public getPathAsSegments(): string[] {
      const entries = Object.entries(this.config.path);
      const url: string[] = [];

      for (const [segment, id] of entries) {
         url.push(segment);
         if (!IS.null(id)) url.push(id + ``);
      }

      return url;
   }


   public getPathAsString(): string {
      return this.getPathAsSegments().join(`/`);
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected clear() {
      delete this._dataRejecter;
      delete this._dataResolver;
      delete this._rawResolver;
      delete this._rawRejecter;
      delete this.raw;
   }
}
// ==================================================
//                   Array
// ==================================================
export class ArrayRequest<T extends object> extends ObjectRequest<TReadonlyArray<T>> {
   constructor(
      readonly config:  IArrayLoad<object>,
   ) {
      super(config);
   }
}
// ==================================================
//                   Merge
// ==================================================
export class MergeRequest<T extends object> extends ArrayRequest<T> {
   constructor(
      readonly config:  IArrayLoad<object>,
      readonly originHash: number,
   ) {
      super(config);
   }
}
