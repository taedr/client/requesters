import { Reactive, IReactive } from '@taedr/reactive';
import { RequesterError } from './errors';


// ==================================================
//                   Model
// ==================================================
export type TShellState = 'INITIAL' | 'READY' | 'ERROR' | 'EMPTY' | 'FULL' | 'LOADING' | 'MERGING';
export type TShellStateIs = TShellState | 'ACTIVE' | 'IDLE' | 'MERGABLE' | 'NO_DATA';

export interface IShellState extends IReactive<TShellState> {
   readonly error: IReactive<RequesterError>;
   is(state: TShellStateIs): boolean;
}
// ==================================================
//                   Class
// ==================================================
export class ShellState extends Reactive<TShellState> implements IShellState {
   readonly error = new Reactive<RequesterError>();
   public merges: number;
   protected _timerId = null;


   constructor() {
      super(`INITIAL`);
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public is(state: TShellStateIs): boolean {
      if (state === 'ACTIVE') return ['LOADING', 'MERGING'].some(s => s === this._value);
      if (state === 'IDLE') return !this.is('ACTIVE');
      if (state === 'MERGABLE') return this._value === `READY`;
      if (state === 'NO_DATA') return ['INITIAL', 'EMPTY', 'ERROR', 'LOADING'].some(s => s === this._value);
      return this._value === state;
   }


   public stopTimer() {
      clearTimeout(this._timerId);
      this._timerId = null;
   }


   public setTimer(id: any) {
      this._timerId = id;
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected send(state: TShellState) {
      this.stopTimer();
      if (this._value === state) return; // #Return#
      if (state !== `ERROR`) this.error.value = null;

      super.send(state);
   }
}
