import { IRequest, IRequester } from '../interfaces/request';
import { IObjectLoad } from '../interfaces/shell';


// ==================================================
//                   Requester
// ==================================================
export abstract class AShellMock implements IRequester {
   protected _timerId = null;

   constructor(
   ) {
   }
   // ---------------------------------------------
   //                Abstract
   // ---------------------------------------------
   readonly abstract isActive: boolean;
   readonly abstract delayMs: number;
   protected abstract getData(req: IObjectLoad<object>): Promise<any>;
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public async load(req: IRequest) {
      this._timerId = setTimeout(async () => {
         try {
            const value = await this.getData(req.config);
            req.resolve(value);
         } catch (e) {
            req.reject(e);
         }
      }, this.delayMs || 100);
   }


   public clear() {
      clearTimeout(this._timerId);
      this._timerId = null;
   }


   public abort() {
      this.clear();
   }
}