import { Bus, IBus, latest } from '@taedr/reactive';
import { IS } from '@taedr/utils';
import { IObjectLoad, IShellCtx } from '../interfaces/shell';
import { RequesterError } from './errors';
import { ShellState, TShellState } from './state';

export interface IShellEvent {
   id: string;
   state: TShellState;
}


export interface IShellErrorEvent extends IShellEvent {
   error: RequesterError;
}


export interface IShellEvents {
   readonly init: IBus<IShellEvent>;
   readonly success: IBus<IShellEvent>;
   readonly error: IBus<IShellErrorEvent>;
   readonly all: IBus<IShellEvent | IShellErrorEvent>;
}


export class ShellEvents implements IShellEvents {
   readonly init = new Bus<IShellEvent>();
   readonly success = new Bus<IShellEvent>();
   readonly error = new Bus<IShellErrorEvent>();
   readonly all = latest([this.init, this.success, this.error]);


   public send(id: string, state: TShellState, error?: RequesterError) {
      const isActive = state === `LOADING` || state === `MERGING`;
      
      if (!IS.null(error)) {
         return this.error.value = { id, state, error };
      }
      
      if (isActive) {
         return this.init.value = { id, state };
      } else {
         return this.success.value = { id, state };
      }
   }
}