import { IId, FN_GET_ID, FN_GET_ID_STR, TReadonlyArray } from '@taedr/utils';
import { _delete, _insert } from '@taedr/arrays';
import { Bus, IBus, List, Reactive } from '@taedr/reactive';
import { IArrayUpdateEntry } from '../interfaces/array';
import { JTC } from '@taedr/jsontoclass';
import { IUpdateEntry } from '@taedr/jsontoclass/utils/update';
import { IIdSync, IRefsSync, ISynchronizable, IUpdateSync, IUpdateEvent, IAddEvent, IDeleteEvent } from '../interfaces/sync';



export interface IShellData<T extends object> {
   getAsArray(): TReadonlyArray<T>;
}
// ==================================================
//                     Object
// ==================================================
export class ActionData<T extends object> extends Bus<T> implements IShellData<T> {
   public getAsArray() {
      return [];
   }
}
// ==================================================
//                     Object
// ==================================================
export class ObjectData<T extends object> extends Reactive<T> implements IShellData<T> {
   public getAsArray() {
      return [this._value].filter(Boolean);
   }
}
// ==================================================
//                     Id
// ==================================================
export class IdObjectData<T extends IId> extends ObjectData<T> implements ISynchronizable<T> {
   // ---------------------------------------------
   //                   Sync
   // ---------------------------------------------   
   public syncUpdate({ syncked, values }: IUpdateSync<T>) {
      const value = this._value;
      const index = values.findIndex(({ id }) => id === value?.id);
      const isNotFound = index === -1;
      const isSyncked = !!syncked[index];

      if (isNotFound) return; // #Return#
      if (isSyncked) return this.value = syncked[index]; // #Return#

      const update: IUpdateEntry<T> = { value, partial: values[index] };

      const [updated] = JTC.update<T>([update], {
         getId: FN_GET_ID,
         isUpdateSymbols: false
      }).values;

      this.value = syncked[index] = updated;
   }


   public syncAdd() {
   }


   public syncDelete({ values, deleted }: IDeleteEvent<T>) {
      const current = this._value;
      const isDelete = values.find(value => value.id === current.id);

      if (isDelete) {
         deleted.add(current);
         this.value = null;
      }

      return this.value === null;
   }


   public getValueById(request: IIdSync<T>) {
      if (request.value !== undefined) return; // #Return#
      const value = this._value;
      request.value = value.id + `` === request.id ? value : undefined;
   }


   public getRefsCount({ counts, values }: IRefsSync<T>) {
      const id = this._value.id;

      for (let i = 0; i < values.length; i++) {
         if (values[i].id !== id) continue; // #Continue#
         counts[i] += 1;
         break;
      }
   }
}
// ==================================================
//                     Array
// ==================================================
export class ArrayData<T extends IId> extends List<T> implements ISynchronizable<T>, IShellData<T> {
   constructor() {
      super(FN_GET_ID_STR);
   }
   // ---------------------------------------------
   //                   Sync
   // ---------------------------------------------   
   public syncUpdate({ syncked, values }: IUpdateEvent<T>) {
      if (this.size === 0) return; // #Return#
      const entries: IArrayUpdateEntry<T>[] = [];

      for (let i = 0; i < values.length; i++) {
         const partial = values[i] as T;
         const index = this._indexByKey.get(partial.id + ``);
         const value = this._value[index] ?? null;
         if (value === null || !!syncked[i]) continue; // #Continue#
         entries.push({ value, partial, index: i });
      }

      if (entries.length) {
         const updated = JTC.update<T>(entries, {
            getId: FN_GET_ID,
            isUpdateSymbols: false
         }).values;

         for (let i = 0; i < entries.length; i++) {
            const { index } = entries[i];
            syncked[index] = updated[i];
         }
      }

      this.update(syncked);
   }


   public syncAdd({ values, position }: IAddEvent<T>) {
      return this.add(values, position);
   }


   public syncDelete({ values, deleted }: IDeleteEvent<T>) {
      this.delete.cross(FN_GET_ID_STR, ...values)
         .map(({ value }) => value)
         .forEach(value => deleted.add(value));

      return this.size === 0;
   }


   public getValueById(request: IIdSync<T>) {
      if (request.value !== undefined) return; // #Return#
      const index = this._indexByKey.get(request.id);
      request.value = this._value[index];
   }


   public getRefsCount({ counts, values }: IRefsSync<T>) {
      for (let i = 0; i < values.length; i++) {
         const id = values[i].id + ``;
         const index = this._indexByKey.get(id);
         const value = this._value[index] ?? null;
         if (value !== null) counts[i] += 1;
      }
   }
   // ---------------------------------------------
   //                   Sync
   // ---------------------------------------------   
   public getAsArray() {
      return this._value;
   }
}
