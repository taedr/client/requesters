import { IId, TReadonlyArray } from '@taedr/utils';
import { IAddEvent, IDeleteEvent, IHashSync, IIdSync, IRefsSync, ISynchronizable, IUpdateEvent } from '../interfaces/sync';
import { ArrayData, IdObjectData, IShellData } from './data';
import { TShellState } from './state';

// ==================================================
//                   Model
// ==================================================
export interface ICacheCtx {
   size: number;
   entryTtlSec: number;
}


export interface ICacheEntry<T extends object> {
   value: T;
   ttlMs: number;
}
// ==================================================
//                   Core
// ==================================================
export class ShellCache<T extends object, C extends IShellData<T>>  {
   protected _hashMap = new Map<number, ICacheEntry<C>>();
   protected _order: number[] = [];
   protected _size: number;
   protected _entryTtlSec: number;

   constructor({
      entryTtlSec,
      size
   }: ICacheCtx) {
      this._size = size;
      this._entryTtlSec = entryTtlSec;
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public add(hash: number, value: C) {
      const ttlMs = Date.now() + this._entryTtlSec * 1000;
      const existing = this._hashMap.get(hash);

      if (!existing) {
         this._hashMap.set(hash, { value, ttlMs });
         this._order.push(hash);
      }

      if (this._hashMap.size > this._size) {
         const firstHash = this._order.shift();
         this._hashMap.delete(firstHash);
      }
   }


   public get(hash: number) {
      const expired = new Set<number>();
      const now = Date.now();
      const _deleted: C[] = [];

      for (const [hash, { ttlMs, value }] of this._hashMap) {
         if (now > ttlMs) {
            expired.add(hash);
            _deleted.push(value);
         }
      }

      if (expired.size) {
         expired.forEach(hash => this._hashMap.delete(hash));
         this._order = this._order.filter(hash => !expired.has(hash));
      }

      const cached = this._hashMap.get(hash)?.value;
      const deleted = _deleted
         .map(del => del.getAsArray())
         .reduce((acc, val) => acc.concat(val), []);

      return { cached, deleted }
   }


   public clear() {
      const deleted: C[] = [];

      for (const [_, { value }] of this._hashMap) {
         deleted.push(value);
      }

      this._hashMap.clear();
      this._order = [];

      const values = deleted
         .map(del => del.getAsArray())
         .reduce((acc, val) => acc.concat(val), []);

      return values;
   }
}
// ==================================================
//                   Sync
// ==================================================
export abstract class ASynckedCache<T extends IId, C extends ISynchronizable<T>> extends ShellCache<T, C> {
   // ---------------------------------------------
   //                Abstract
   // ---------------------------------------------
   public abstract getValueByHash(request: IHashSync<T>): void;
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public syncUpdate(sync: IUpdateEvent<T>) {
      for (const [_, { value }] of this._hashMap) {
         value.syncUpdate(sync);
      }
   }


   public syncAdd(hash: number, sync: IAddEvent<T>) {
      this._hashMap.get(hash)?.value.syncAdd(sync);
   }


   public syncDelete(sync: IDeleteEvent<T>) {
      const cleared = new Set<number>();

      for (const [hash, { value }] of this._hashMap) {
         const isCleared = value.syncDelete(sync);
         if (isCleared) cleared.add(hash);
      }

      for (const hash of cleared) {
         this._hashMap.delete(hash);
      }
   }


   public getValueById(request: IIdSync<T>) {
      for (const [_, { value }] of this._hashMap) {
         value.getValueById(request);
      }
   }


   public getRefsCount(request: IRefsSync<T>) {
      for (const [_, { value }] of this._hashMap) {
         value.getRefsCount(request);
      }
   }
}
// ==================================================
//                   Id
// ==================================================
export class IdObjectCache<T extends IId> extends ASynckedCache<T, IdObjectData<T>> {
   public getValueByHash(request: IHashSync<T>) {
      if (request.entry !== undefined) return; // #Return#
      const value = this._hashMap.get(request.hash)?.value;
      if (!value) return; // #Return#
      request.entry = { value, state: `READY`, merges: 0 }
   }
}
// ==================================================
//                   Array
// ==================================================
export class ArrayCache<T extends IId> extends ASynckedCache<T, CachedArrayData<T>> {
   public getValueByHash(request: IHashSync<T>) {
      if (request.entry !== undefined) return; // #Return#
      request.entry = this._hashMap.get(request.hash)?.value;
   }
}


export class CachedArrayData<T extends IId> extends ArrayData<T> {
   public state: TShellState;
   public merges = 1;


   constructor(
      cached: TReadonlyArray<T>
   ) {
      super();
      this.add(cached);
   }
}