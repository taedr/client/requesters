import { JtcResult } from '@taedr/jsontoclass';
import { TNew } from '@taedr/utils';
import { TShellState } from './state';

export interface IError {
   readonly code: number;
   readonly message: string;
}


export class RequesterError implements IError {
   constructor(
      readonly code: number,
      readonly message: string,
   ) { }
}


export class CorruptionError extends RequesterError {
   constructor(
      public result: JtcResult<object>,
   ) {
      super(5, `Data is not in expected shape`);
   }
}

export class ExpiredError extends RequesterError {
   constructor(
      public ttlMs: number,
   ) {
      super(6, `Server is not responding`);
   }
}
export class ShapeError extends RequesterError {
   constructor(
      public raw: unknown,
      public message: string
   ) {
      super(13, message);
   }
}


function e(code: number, message: string) {
   return new RequesterError(code, message);
}


export const ERRORS = {
   null: e(404, `Response is empty`),
   corruption: (result: JtcResult<object>) => new CorruptionError(result),
   expired: (ttlMs: number) => new ExpiredError(ttlMs),
   noInternet: e(8, `No internet connection`),
   abort: e(9, `Request aborted`),
   merge: (state: TShellState) => `Can't merge from '${state}' state`,
   load: (state: TShellState) => `Can't load, shell is already in '${state}' state`,
   partialUpdateId: e(11, `Can't update data if partial doesn't have an 'id'`),
   shouldNull: (raw: unknown) => new ShapeError(raw, `Expected empty response, but got data`),
   array: (raw: unknown) => new ShapeError(raw, `Expected array, but got object`),
   object: (raw: unknown) => new ShapeError(raw, `Expected object, but got array`),
   initial: (raw: unknown) => new ShapeError(raw, `Got duplicate ids in initial array`),
   action: {
      shard: (raw: unknown) => new ShapeError(raw, `Action 'shardId' is empty`),
      id: (empty: unknown[]) => new ShapeError(empty, `Action values with missing 'id'`),
      duplicate: (model: TNew<object>) => new Error(
         `Model with name '${model.name}' is alredy used in another ShellActionSync instance.` +
         `Each model can only be used once`
      )
   }
};
