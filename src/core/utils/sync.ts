import { IId, TNew, TReadonlyArray } from '@taedr/utils';
import { IBus, Bus, } from '@taedr/reactive';
import { IUpdateSync, IDeleteSync, IIdSync, IHashSync, IRefsSync, ISyncEvent, IHashEntry, IAddSync } from '../interfaces/sync';
import { TPosition } from '@taedr/arrays';
import { AShell } from '../classes/shell';

// ==================================================
//                   Values
// ==================================================
export class ShellSync<T extends IId> {
   protected _bus = new Bus<ISyncEvent>();

   public get bus(): IBus<ISyncEvent> { return this._bus; }

   constructor() { }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public add(
      values: TReadonlyArray<T>,
      position: TPosition,
      initiator: object,
      path: string[],
   ) {
      const event: IAddSync<T> = {
         type: `ADD`,
         path,
         values,
         initiator,
         position
      };

      this._bus.value = event;
   }


   public update(
      values: TReadonlyArray<T>,
      initiator: object,
      path: string[],
   ) {
      const syncked: T[] = [];
      const event: IUpdateSync<T> = {
         type: `UPDATE`,
         path,
         syncked,
         values,
         initiator,
      };

      this._bus.value = event;

      const updated = values.map((value, i) => !!syncked[i] ? syncked[i] : value);

      return updated;
   }


   public delete(
      values: TReadonlyArray<T>,
      initiator: object,
      path: string[],
   ) {
      const deleted = new Set<T>();
      const event: IDeleteSync<T> = {
         type: `DELETE`,
         deleted,
         values,
         path,
         initiator
      };

      this._bus.value = event;

      return deleted;
   }


   public getById(
      id: string,
      initiator: object,
      path: string[],
   ): T | undefined {
      const event: IIdSync<T> = {
         type: `ID`,
         value: undefined,
         path,
         id,
         initiator,
      };

      this._bus.value = event;

      return event.value;
   }


   public getByHash(
      hash: number,
      initiator: object,
      path: string[],
   ): IHashEntry | undefined {
      const event: IHashSync<T> = {
         type: `HASH`,
         entry: undefined,
         path,
         hash,
         initiator,
      };

      this._bus.value = event;

      return event.entry;
   }


   public getRefsCount(
      values: TReadonlyArray<T>,
      initiator: object,
      counts: number[],
      path: string[],
   ): number[] {
      const event: IRefsSync<T> = {
         type: `REFS`,
         values,
         counts,
         path,
         initiator,
      };

      this._bus.value = event;

      return counts;
   }
}
// ==================================================
//                   Global
// ==================================================
export class ShellSyncGlobal {
   protected _syncByModel = new Map<TNew<IId>, ShellSync<IId>>();

   constructor() {

   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public get<T extends IId>(model: TNew<T>): ShellSync<T> {
      const existings = this._syncByModel.get(model) as ShellSync<T>;

      if (existings) return existings; // #Return#

      const sync = new ShellSync<T>()
      this._syncByModel.set(model, sync);

      return sync;
   }


   public add<T extends IId>(
      values: TReadonlyArray<T>,
      position: TPosition,
      path: string[],
      initiator: object,
   ) {
      const byBuilder = this.mapByBuilder(values);

      for (const [model, array] of byBuilder) {
         const sync = this.get(model);
         sync.add(array, position, initiator, path);
      }
   }


   public delete<T extends IId>(
      values: TReadonlyArray<T>,
      path: string[],
      initiator: object,
   ) {
      const byBuilder = this.mapByBuilder(values);
      const deleted: T[] = [];

      for (const [model, array] of byBuilder) {
         const sync = this.get(model);
         const _deleted = sync.delete(array, initiator, path);
         deleted.push(..._deleted);
      }

      this.clearInnerShells(deleted, path);
   }


   public update<T extends IId>(
      values: TReadonlyArray<T>,
      path: string[],
      initiator: object,
   ) {
      const byBuilder = this.mapByBuilder(values);

      for (const [model, array] of byBuilder) {
         const sync = this.get(model);
         sync.update(array, initiator, path);
      }
   }


   public clearInnerShells<T extends object>(
      values: TReadonlyArray<T>,
      path: string[],
   ) {
      if (values.length === 0) return; // #Return#

      const byBuilder = this.mapByBuilder(values);

      for (const [builder, values] of byBuilder) {
         this.clearModel(builder, values, path);
      }
   }
   // ---------------------------------------------
   //                Inners
   // ---------------------------------------------
   protected mapByBuilder<T extends object>(
      values: TReadonlyArray<T>,
   ): Map<TNew<T>, T[]> {
      const byBuilder = new Map<TNew<T>, T[]>();

      for (const value of values) {
         const builder = value['constructor'] as TNew<T>;
         const array = byBuilder.get(builder) ?? [];
         array.push(value);
         byBuilder.set(builder, array);
      }

      return byBuilder;
   }


   protected clearModel<T extends object>(
      model: TNew<T>,
      values: TReadonlyArray<T>,
      path: string[],
   ) {
      const counts = new Array(values.length).fill(0);
      const sync = this.get(model as any);

      if (sync) {
         sync.getRefsCount(values as any, this, counts, path);
      }

      for (let i = 0; i < counts.length; i++) {
         if (counts[i] > 0) continue; // #Continue#

         const value = values[i]
         const symbols = Object.getOwnPropertySymbols(value)
            .filter(symbol => value[symbol] instanceof AShell);

         for (const symbol of symbols) {
            value[symbol].clear();
         }
      }
   }
}

export const SHELL_SYNC_GLOBAL = new ShellSyncGlobal();