import { IId, FN_GET_ID, IS, TReadonlyArray, getPack } from '@taedr/utils';
import { IArrayShellState, IArrayShellCtx, IArrayLoad } from '../interfaces/array';
import { ERRORS } from '../utils/errors';
import { TShellState, ShellState } from '../utils/state';
import { IConvertConfig } from '@taedr/jsontoclass/core/converter';
import { IHashEntry, IAddEvent, IDeleteEvent, IUpdateEvent } from '../interfaces/sync';
import { IMergeRequest, MergeRequest, ArrayRequest } from '../utils/request';
import { ArrayData } from '../utils/data';
import { IList, TWatcher } from '@taedr/reactive';
import { ASyncShell } from './sync';
import { ArrayCache, CachedArrayData, ICacheCtx } from '../utils/cache';
import { SHELL_SYNC_GLOBAL } from '../utils/sync';


// ==================================================
//                   Shell
// ==================================================
export class ArrayShell<T extends IId> extends ASyncShell<T, TReadonlyArray<T>> {
   protected _data = new ArrayData<T>();
   protected _state = new ShellState();
   protected _cache: ArrayCache<T>;
   protected _request: ArrayRequest<T>;

   public get data(): IList<T> { return this._data; }
   public get state(): IArrayShellState { return this._state; }

   constructor(
      protected _ctx: IArrayShellCtx<T>
   ) {
      super(_ctx);
      this._state.merges = 0;
      this.init();
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public load(config: IArrayLoad<object>) {
      this._state.merges = 0;
      return super.load(config);
   }


   public clear() {
      this._state.merges = 0;
      super.clear();
   }


   public merge(config: IArrayLoad<object>): IMergeRequest<T> {
      const state = this._state.value;
      const isNotMergable = !this._state.is(`MERGABLE`);

      if (isNotMergable) throw new Error(ERRORS.merge(state)); // #Error#

      const rq = this._request;
      const originHash = rq instanceof MergeRequest ? rq.originHash : rq.hash;
      const request = new MergeRequest<T>(config, originHash);

      this.reload(`MERGING`, request);

      return request;
   }
   // ---------------------------------------------
   //                Init
   // ---------------------------------------------
   protected init() {
      super.init();
   }


   protected initDebug() {
      const watcher = (type: string): TWatcher<any> => value => {
         const segments = [this._ctx.id, type].join(` - `);
         console.log(`${segments}:`, value);
      }

      this._state.watch(watcher(`STATE`));
      this._data.on.delete.watch(watcher(`DELETE`));
      this._data.on.update.watch(watcher(`UPDATE`));
      this._data.on.add.watch(watcher(`ADD`));
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected setValue(data: T[]) {
      const state = this._state.value;

      if (state === `MERGING`) {
         const request = this._request as MergeRequest<T>;
         this._state.merges += 1;
         this._data.add(data, request.config.position);
      } else {
         this._state.merges = 1;
         this._data.value = data;
      }

      return this.getState(data);
   }


   protected isValidShape(raw: object) {
      if (!IS.array(raw)) return ERRORS.array(raw);
   }


   protected getState(data: T[]): TShellState {
      const isEmpty = this._data.size === 0 && data.length === 0;
      const isFull = data.length < this._request.config.packSize;

      if (isEmpty) return `EMPTY`;
      if (isFull) return `FULL`;
      return `READY`;
   }


   protected getConverterConfig(): IConvertConfig<T> {
      return { ...super.getConverterConfig(), getId: FN_GET_ID };
   }
   // ---------------------------------------------
   //                Sync
   // ---------------------------------------------
   protected onAdd(event: IAddEvent<T>) {
      const isEmpty = this._data.size === 0;
      const { isChanged } = this._data.syncAdd(event);

      this._cache?.syncAdd(this._request.hash, event);

      if (isEmpty && isChanged) this._state.value = `READY`;
   }


   protected onDelete(event: IDeleteEvent<T>) {
      const isCleared = this._data.syncDelete(event);
      const isFull = this._state.value === `FULL`;

      this._cache?.syncDelete(event);

      if (isCleared && isFull) this._state.value = `EMPTY`;
   }


   protected onUpdate(event: IUpdateEvent<T>) {
      this._data.syncUpdate(event);
   }
   // ---------------------------------------------
   //                Cache
   // ---------------------------------------------
   protected createCache(ctx: ICacheCtx) {
      return new ArrayCache<T>(ctx);
   }


   protected tryToGetCache() {
      if (this._request instanceof MergeRequest) return false; // #Return#

      const models = getPack(this._ctx.models);
      const cached = this._cache?.get(this._request.hash);
      let entry: IHashEntry;


      if (cached) {
         this.clearInnerShells(cached.deleted);
         entry = cached.cached;
      } else if (this._request.config.isCheckSync ?? true) {
         for (const model of models) {
            const sync = SHELL_SYNC_GLOBAL.get(model);
            entry = sync.getByHash(this._request.hash, this, this._request.config.path);
            if (entry) break;
         }
      }

      if (!entry) return false; // #Return#

      const value = entry.value as T[];

      this.setValue(value);
      this._state.value = entry.state;
      this._state.merges = entry.merges;
      this._request.resolveData(value);

      return true;
   }


   protected setCache(value: TReadonlyArray<T>) {
      const request = this._request;

      if (request instanceof MergeRequest) {
         const { cached, deleted } = this._cache.get(request.originHash);
         this.clearInnerShells(deleted);

         if (!cached) return; // #Return#

         cached.state = this._state.value;
         cached.merges = this._state.merges;
         cached.add(value, request.config.position);
      } else {
         const cache = new CachedArrayData(value);
         cache.state = this._state.value;
         this._cache.add(request.hash, cache);
      }
   }
}

