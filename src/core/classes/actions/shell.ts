import { TAIL } from '@taedr/arrays';
import { Bus, Collection, IBus, ICollection, IReactive, Reactive } from '@taedr/reactive';
import { IId, IS } from '@taedr/utils';
import { IActionError, IActionPending, IActionsShellCtx, IActionSuccess, IPerform, ISyncActionsShellCtx, TActionsState } from '../../interfaces/action';
import { IShellState } from '../../utils/state';
import { SHELL_SYNC_GLOBAL } from '../../utils/sync';
import { AActionShell } from './performers/core';

// ==================================================
//                   Action
// ==================================================
export class ActionsShell<T extends object, M extends object>  {
   protected _pending = new Collection<IActionPending<T>>();
   protected _success = new Bus<IActionSuccess<T, M>>();
   protected _error = new Bus<IActionError>();
   protected _state = new Reactive<TActionsState>(`INITIAL`);

   public get pending(): ICollection<IActionPending<T>> { return this._pending; }
   public get state(): IReactive<TActionsState> { return this._state; }
   public get error(): IBus<IActionError> { return this._error; }
   public get success(): IBus<IActionSuccess<T, M>> { return this._success; }

   constructor(
      protected _ctx: IActionsShellCtx<T, M>
   ) { }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public perform(config: IPerform<object>): IActionPending<T> {
      const index = this._pending.size;
      const shell = this._ctx.getPerformer(index);
      const value = shell.load(config).value;
      const state = shell.state;
      const action: IActionPending<T> = {
         config,
         state,
         value,
         abort: () => shell.clear(),
      };

      this._state.value = `LOADING`;
      this._pending.add([action]);

      value
         .then(value => this.onSuccess(config, value, state))
         .catch(() => this.onError(config, state))
         .finally(() => this.onFinally(shell, action));

      return action;
   }
   // ---------------------------------------------
   //                Events
   // ---------------------------------------------
   protected onSuccess(config: IPerform<object>, value: T, state: IShellState) {
      this._success.value = { config, value, state, mapped: null };
   }


   protected onError(config: IPerform<object>, state: IShellState) {
      this._error.value = { config, state };
   }


   protected onFinally(shell: AActionShell<M, object>, action: IActionPending<T>) {
      shell['_state'] = null;
      this._pending.delete.values(action);
      if (this._pending.size === 0) {
         this._state.value = `READY`;
      }
   }

}
// ==================================================
//                   Sync
// ==================================================
export class SyncActionsShell<T extends object, M extends IId> extends ActionsShell<T, M> {
   constructor(
      protected _ctx: ISyncActionsShellCtx<T, M>
   ) {
      super(_ctx);
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   protected onSuccess(config: IPerform<object>, value: T, state: IShellState) {
      const { type } = this._ctx;
      const path = config.path;
      const initiator = this;
      const values = (IS.array(value) ? value : [value]) as M[];

      switch (type) {
         case `ADD`: SHELL_SYNC_GLOBAL.add(values, TAIL, path, initiator); break;
         case `DELETE`: SHELL_SYNC_GLOBAL.delete(values, path, initiator); break;
         case `UPDATE`: SHELL_SYNC_GLOBAL.update(values, path, initiator); break;
      }

      super.onSuccess(config, value, state);
   }
}