

import { IS, TReadonlyArray } from '@taedr/utils';
import { IArrayActionShellCtx, IPerform } from '../../../interfaces/action';
import { ERRORS } from '../../../utils/errors';
import { TShellState } from '../../../utils/state';
import { AActionShell } from './core';


// ==================================================
//                   Shell
// ==================================================
export class ArrayActionShell<T extends object> extends AActionShell<T, TReadonlyArray<T>> {
   constructor(
      ctx: IArrayActionShellCtx<T>
   ) {
      super(ctx);
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected isValidShape(raw: object) {
      if (!IS.array(raw)) return ERRORS.array(raw);
   }


   protected setValue(values: T[]): TShellState {
      this._data.value = values;

      return values.length === 0 ? `EMPTY` : `READY`;
   }
}