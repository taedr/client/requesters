import { IS } from '@taedr/utils';
import { IObjectActionShellCtx } from '../../../interfaces/action';
import { ERRORS } from '../../../utils/errors';
import { TShellState } from '../../../utils/state';
import { AActionShell } from './core';

// ==================================================
//                   Shell
// ==================================================
export class ObjectActionShell<T extends object> extends AActionShell<T, Readonly<T>> {
   constructor(
      ctx: IObjectActionShellCtx<T>
   ) {
      super(ctx)
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected isValidShape(raw: object) {
      if (IS.array(raw)) return ERRORS.object(raw);
   }


   protected setValue([value]: T[]): TShellState {
      this._data.value = value;
      return `READY`;
   }
}
