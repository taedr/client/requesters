import { IActionShellCtx } from '../../../interfaces/action';
import { ActionData } from '../../../utils/data';
import { ShellState } from '../../../utils/state';
import { AShell } from '../../shell';


// ==================================================
//                   Shell
// ==================================================
export abstract class AActionShell<M extends object, T extends object> extends AShell<M, T> {
   protected _data = new ActionData<T>();
   protected _state = new ShellState();

   constructor(
      protected ctx: IActionShellCtx<M>,
   ) {
      super(ctx);
      this.init();
   }
   // ---------------------------------------------
   //                Cache
   // ---------------------------------------------
   protected createCache() { return null; }
   protected tryToGetCache() { return false; }
   protected setCache() { }
}