import { Bus, IReactive, TWatcher } from '@taedr/reactive';
import { JtcResult, JsonToClass } from '@taedr/jsontoclass';
import { Hub, TReadonlyArray } from '@taedr/utils';
import { ERRORS, RequesterError } from '../utils/errors';
import { IObjectLoad, IShell, IShellCtx } from '../interfaces/shell';
import { IShellState, ShellState, TShellState } from '../utils/state';
import { IConvertConfig } from '@taedr/jsontoclass/core/converter';
import { IObjectRequest, ObjectRequest } from '../utils/request';
import { IShellEvents, ShellEvents } from '../utils/events';
import { ICacheCtx, ShellCache } from '../utils/cache';
import { SHELL_SYNC_GLOBAL } from '../utils/sync';
import { IShellData } from '../utils/data';

// ==================================================
//                   Shell
// ==================================================
export abstract class AShell<M extends object, T extends object> implements IShell<T> {
   protected _hub = new Hub(`OFF`);
   protected _request: ObjectRequest<T>;
   protected _cache: ShellCache<M, IShellData<M>>;

   public get data(): IReactive<T> { return this._data; }
   public get state(): IShellState { return this._state; }
   public get id(): string { return this._ctx.id; }

   constructor(
      protected _ctx: IShellCtx<M>,
   ) {

   }
   // ---------------------------------------------
   //                Abstract
   // ---------------------------------------------
   protected abstract _data: Bus<T> & IShellData<M>;
   protected abstract _state: ShellState;
   protected abstract setValue(data: M[]): TShellState;
   protected abstract isValidShape(raw: object): RequesterError | undefined;

   protected abstract createCache(ctx: ICacheCtx): ShellCache<M, IShellData<M>>;
   protected abstract setCache(value: T): void;
   protected abstract tryToGetCache(): boolean;
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public load(config: IObjectLoad<object>): IObjectRequest<T> {
      const state = this._state.value;
      const isNotInitial = state !== `INITIAL` && state !== `ERROR`;
      const request = new ObjectRequest<T>(config);
      const array = this._data.getAsArray();

      if (config.isOnlyIfInitial && isNotInitial) return this._request; // #Return#
     
     
      this.clearInnerShells(array);
      this._data.value = null;

      this.reload(`LOADING`, request);
      return this._request;
   }


   public clear() {
      this._hub.state = `OFF`;
      this._state.stopTimer();

      if (this._request) {
         const curr = this._data.getAsArray();
         const cached = this._cache?.clear() ?? [];
         const values = [...curr, ...cached];
   
         this._ctx.requester.abort(this._request);
         this.clearInnerShells(values);
         delete this._request;
      }

      this._state.value = 'INITIAL';
      this._data.value = null;
   }
   // ---------------------------------------------
   //                Init
   // ---------------------------------------------
   protected init() {
      if (this._ctx.cache) this._cache = this.createCache(this._ctx.cache);
      if (this._ctx.mock?.isActive) this._ctx.requester = this._ctx.mock;
      delete this._ctx.mock;
      delete this._ctx.cache;

      if (this._ctx.isDebug) {
         this.initDebug();
      }
   }


   protected initDebug() {
      const watcher: TWatcher<any> = value => console.log(`${this._ctx.id} -`, value);
      this._state.watch(watcher);
      this._data.watch(watcher);
   }
   // ---------------------------------------------
   //                Reload
   // ---------------------------------------------
   protected reload(state: TShellState, request: ObjectRequest<T>) {
      if (this._state.is(`ACTIVE`)) { // #Error#
         throw new Error(ERRORS.load(this._state.value));
      }

      this._hub.state = `ON`;
      this._state.value = state;
      this._request = request;
      this.sendEvent();

      const ttl = this._ctx.ttlSec * 1000;
      const isGotCached = this.tryToGetCache();

      if (isGotCached) {
         this.sendEvent();
         return; // #Return#
      }

      this._state.setTimer(setTimeout(() => {
         this._ctx.requester.abort(request);
         request.reject(ERRORS.expired(ttl));
      }, ttl));

      request.raw
         .then(raw => this.onIncome(raw))
         .catch(e => this.onError(e));

      this._ctx.requester.load(request);
   }
   // ---------------------------------------------
   //                Events
   // ---------------------------------------------
   protected onIncome(raw: any) {
      const shape = this.isValidShape(raw);

      if (!!shape) return this.onError(shape); // #Return#

      const result = this.mapValues(raw)
      const data = result.data.all;

      if (result.isCorrupted) {
         this.onCorruption(data, result);
      } else {
         this.setData(data);
      }
   }


   protected mapValues(raw: any) {
      const jtc = new JsonToClass(this._ctx.models, this._ctx.id);
      const config = this.getConverterConfig();
      const result = jtc.convert(raw, config);
      return result;
   }


   protected onError(e: any) {
      let error: RequesterError;

      if (e instanceof RequesterError) {
         error = e;
      } else {
         const code = e['code'] ?? 500;
         const message = e['message'] ?? `Unknown Error`;
         error = new RequesterError(code, message);
      }

      console.error(e);

      this._state.error.value = error;
      this._state.value = `ERROR`;
      this._request?.rejectData(error);
      this.sendEvent();
   }


   protected onCorruption(data: M[], result: JtcResult<M>) {
      const error = ERRORS.corruption(result);

      switch (this._ctx.SCorruption) {
         case `REPAIR`: return this.setData(data);
         case `FORBID`: default: return this.onError(error);
      }
   }
   // ---------------------------------------------
   //                Utils
   // ---------------------------------------------
   protected setData(data: M[]) {
      const state = this.setValue(data);
      const syncked = this._data.value;
      this._request.resolveData(syncked);
      this._state.value = state;
      if (this._cache) this.setCache(syncked);
      this.sendEvent();
   }


   protected getConverterConfig(): IConvertConfig<M> {
      return {
         SUnknownKey: this._ctx.SUnknownKeys ?? `ERROR`,
         isPartial: this._ctx.SStructure === `PARTIAL`,
         isFreeze: true,
      }
   }


   protected sendEvent() {
      AShell._on.send(this._ctx.id, this._state.value, this._state.error.value);
   }


   protected clearInnerShells(values: TReadonlyArray<M>) {
      if (!this._request) return; // #Return#
      SHELL_SYNC_GLOBAL.clearInnerShells(values, this._request.config.path);
   }
   // ---------------------------------------------
   //                Static
   // ---------------------------------------------
   protected static _on = new ShellEvents();
   public static get on(): IShellEvents { return AShell._on; }
}
