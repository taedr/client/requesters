import { AShell } from './shell';
import { IObjectShellCtx, IIdObjectShellCtx, IIdObjectLoad } from '../interfaces/object';
import { IS, IId, getPack } from '@taedr/utils';
import { TShellState, ShellState } from '../utils/state';
import { ERRORS } from '../utils/errors';
import { IUpdateSync, IDeleteEvent } from '../interfaces/sync';
import { ObjectData, IdObjectData } from '../utils/data';
import { ASyncShell } from './sync';
import { ICacheCtx, IdObjectCache, ShellCache } from '../utils/cache';
import { SHELL_SYNC_GLOBAL } from '../utils/sync';
import { IObjectRequest } from '../utils/request';

// ==================================================
//                     Plain
// ==================================================
export class ObjectShell<T extends object> extends AShell<T, Readonly<T>> {
   protected _data = new ObjectData<T>();
   protected _state = new ShellState();
   protected _cache: ShellCache<T, ObjectData<T>>;

   constructor(
      protected _ctx: IObjectShellCtx<T>,
   ) {
      super(_ctx);
      super.init();
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected setValue([value]: T[]): TShellState {
      this._data.value = value;

      return `READY`;
   }


   protected isValidShape(raw: object) {
      if (IS.array(raw)) return ERRORS.object(raw);
   }
   // ---------------------------------------------
   //                Cache
   // ---------------------------------------------
   protected createCache(ctx: ICacheCtx) {
      return new ShellCache<T, ObjectData<T>>(ctx);
   }


   protected tryToGetCache() {
      const { cached, deleted } = this._cache?.get(this._request.hash) ?? {};

      if (deleted) this.clearInnerShells(deleted);
      if (!cached) return false; // #Return#

      this._data.value = cached.value;
      this._state.value = `READY`;

      return true;
   }


   protected setCache(value: T) {
      this._cache.add(this._request.hash, new ObjectData(value));
   }
}
// ==================================================
//                     Id
// ==================================================
export class IdObjectShell<T extends IId> extends ASyncShell<T, Readonly<T>> {
   protected _data = new IdObjectData<T>();
   protected _state = new ShellState();
   protected _cache: IdObjectCache<T>;

   constructor(
      protected _ctx: IIdObjectShellCtx<T>
   ) {
      super(_ctx);
      this.init();
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public load(config: IIdObjectLoad<object>): IObjectRequest<T> {
      return super.load(config);
   }
   // ---------------------------------------------
   //                Internal
   // ---------------------------------------------
   protected isValidShape(raw: object) {
      if (IS.array(raw)) return ERRORS.object(raw);
   }


   protected setValue([value]: T[]): TShellState {
      this._data.value = value;

      return `READY`;
   }
   // ---------------------------------------------
   //                Events
   // ---------------------------------------------
   protected onAdd() { }

   protected onDelete(sync: IDeleteEvent<T>) {
      const isCleared = this._data.syncDelete(sync);
      this._cache?.syncDelete(sync);
      if (isCleared) {
         this._state.value = 'INITIAL';
         this._data.value = null;
      }
   }


   protected onUpdate(sync: IUpdateSync<T>) {
      this._cache?.syncUpdate(sync);
      if (this.isSkipSync(sync)) return; // #Return#
      this._data.syncUpdate(sync);
   }
   // ---------------------------------------------
   //                Cache
   // ---------------------------------------------
   protected createCache(ctx: ICacheCtx) {
      return new IdObjectCache<T>(ctx);
   }


   protected tryToGetCache() {
      const id = this._request.config['id'] + ``;
      const models = getPack(this._ctx.models);
      const cached = this._cache?.get(this._request.hash);
      let entry: T;


      if (cached) {
         this.clearInnerShells(cached.deleted);
         entry = cached.cached.value;
      } else if (this._request.config.isCheckSync ?? true) {
         for (const model of models) {
            const sync = SHELL_SYNC_GLOBAL.get(model);
            entry = sync.getById(id, this, this._request.config.path) as T;
            if (entry) break;
         }
      }

      if (!entry) return false; // #Return#

      this.setValue([entry]);
      this._state.value = `READY`;
      this._request.resolveData(entry);

      return true;
   }


   protected setCache(value: T) {
      this._cache.add(this._request.hash, new IdObjectData(value));
   }
}