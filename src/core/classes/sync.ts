import { JsonToClass } from '@taedr/jsontoclass';
import { Reactive } from '@taedr/reactive';
import { getPack, IId, TNew, TReadonlyArray } from '@taedr/utils';
import { IObjectLoad, ISyncShellCtx } from '../interfaces/shell';
import { IAddEvent, IAddSync, IDeleteEvent, IDeleteSync, IHashSync, IIdSync, IRefsSync, IChanges, ISyncEvent, ISynchronizable, IUpdateEvent, IUpdateSync } from '../interfaces/sync';
import { ASynckedCache } from '../utils/cache';
import { SHELL_SYNC_GLOBAL } from '../utils/sync';
import { AShell } from './shell';

export abstract class ASyncShell<M extends IId, T extends object> extends AShell<M, T> {
   protected _cache: ASynckedCache<M, ISynchronizable<M>>;

   constructor(
      protected _ctx: ISyncShellCtx<M>,
   ) {
      super(_ctx);
   }

   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public load(config: IObjectLoad<object>) {
      const isWs = !!this._ctx.requester.changes;
      const isCheckSync = isWs ? false : config.isCheckSync;
      return super.load({ ...config, isCheckSync });
   }
   // ---------------------------------------------
   //                Abstract
   // ---------------------------------------------
   protected abstract _data: ISynchronizable<M> & Reactive<T>;
   protected abstract onAdd(sync: IAddEvent<IId>): void;
   protected abstract onDelete(sync: IDeleteEvent<IId>): void;
   protected abstract onUpdate(sync: IUpdateEvent<IId>): void;
   // ---------------------------------------------
   //                Init
   // ---------------------------------------------
   protected init() {
      this.watchSync();
      super.init();
      this.watchChanges();
   }


   protected watchSync() {
      const models = getPack(this._ctx.models);

      for (const model of models) {
         SHELL_SYNC_GLOBAL.get(model).bus.watch({
            hub: this._hub,
            value: sync => this.onEvent(sync)
         });
      }
   }


   protected watchChanges() {
      this._ctx.requester.changes?.watch({ 
         hub: this._hub, 
         value: changes => this.onChanges(changes) 
      });
   }


   protected onEvent(sync: ISyncEvent) {
      if (this.isSkipSync(sync)) return; //#Return#

      switch (sync.type) {
         case `UPDATE`: return this.onUpdate(sync as IUpdateSync<M>);
         case `REFS`: return this.onGetRefsCount(sync as IRefsSync<M>);
         case `ID`: return this.onGetValueById(sync as IIdSync<M>);
         case `HASH`: return this.onGetValueByHash(sync as IHashSync<M>);
         case `ADD`: return this.onAdd(sync as IAddSync<M>);
         case `DELETE`: return this.onDelete(sync as IDeleteSync<M>);
      }
   }


   protected onChanges({ added, updated, deleted }: IChanges) {
      const id = `${this._ctx.id} - ws change`;
      const models = this._ctx.models;
      const jtc = new JsonToClass(models, id);
      const config = this.getConverterConfig();

      if (deleted.length) {
         this.onDelete({ values: deleted, deleted: new Set() });
      }

      if (updated.length) {
         const mapped = jtc.convert(updated, { ...config, isPartial: true });

         if (mapped.isCorrupted) throw new Error(``); // #Error#

         const syncked = this.syncValues(mapped.data.all);
         this.onUpdate({ syncked, values: syncked });
      }

      if (added.length) {
         const _added = added.map(({ value }) => value);
         const mapped = jtc.convert(_added, config);

         if (mapped.isCorrupted) throw new Error(``); // #Error#

         const syncked = this.syncValues(mapped.data.all);

         for (let i = 0; i < added.length; i++) {
            const position = added[i].index;
            const values = [syncked[i]];

            this.onAdd({ values, position });
         }
      }
   }


   protected setData(data: M[]) {
      const syncked = this.syncValues(data);
      super.setData(syncked)
   }
   // ---------------------------------------------
   //                Events
   // ---------------------------------------------
   protected onGetRefsCount(sync: IRefsSync<M>) {
      this._cache?.getRefsCount(sync);
      if (this.isSkipSync(sync)) return; // #Return#
      this._data.getRefsCount(sync);
   }


   protected onGetValueById(request: IIdSync<M>) {
      this._cache?.getValueById(request);
      if (this.isSkipSync(request)) return; // #Return#
      this._data.getValueById(request);
   }


   protected onGetValueByHash(request: IHashSync<M>) {
      this._cache?.getValueByHash(request);
      if (this.isSkipSync(request)) return; // #Return#
      const isCurr = this._request?.hash === request.hash;
      if (!isCurr || !!request.entry) return; // #Return#

      request.entry = {
         value: this._data.value,
         merges: this._state.merges,
         state: this._state.value,
      }
   }
   // ---------------------------------------------
   //                Syncker
   // ---------------------------------------------
   protected isSyncable({ initiator, type }: ISyncEvent) {
      const isCrud = type === `ADD` || type === `DELETE`;
      const isWs = !!this._ctx.requester.changes;
      const isExteranlCrud = isWs && isCrud;
      const isInitiator = initiator === this;
      const isNoData = this.state.is(`NO_DATA`);

      if (isInitiator || isNoData || isExteranlCrud) return false; // #Return#

      return true;
   }


   protected isSkipSync(sync: ISyncEvent) {
      return !this.isSyncable(sync);
   }


   protected syncValues(values: M[]): M[] {
      const models = this._ctx.models;

      if (models.length === 1) {
         const sync = SHELL_SYNC_GLOBAL.get(models[0]);
         const syncked = sync.update(values, this, this._request.config.path);
         this._cache?.syncUpdate({ values, syncked });

         return syncked;
      } else {
         return this.syncValuesByModel(models, values);
      }
   }


   protected syncValuesByModel(models: TNew<M>[], values: TReadonlyArray<M>) {
      const syncked: M[] = [];
      const valueByBuilder = new Map<TNew<M>, M[]>();
      const indexById = new Map<string | number, number>();

      for (const model of models) {
         valueByBuilder.set(model, []);
      }

      for (let i = 0; i < values.length; i++) {
         const value = values[i];
         const builder = value['constructor'] as TNew<M>;

         indexById.set(value.id, i);
         valueByBuilder.get(builder).push(value);
      }

      for (const [model, values] of valueByBuilder) {
         const sync = SHELL_SYNC_GLOBAL.get(model);
         const synckedByModel = sync.update(values, this, this._request.config.path);

         this._cache?.syncUpdate({ values, syncked: synckedByModel });

         for (const value of synckedByModel) {
            const index = indexById.get(value.id);
            syncked[index] = value;
         }
      }

      return syncked;
   }
}