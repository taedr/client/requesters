import { IId } from '@taedr/utils';
import { IObjectLoad, ISyncShellCtx } from './shell';
import { IShellState } from '../utils/state';
import { TPosition } from '@taedr/arrays';


export interface IArrayShellCtx<T extends IId> extends ISyncShellCtx<T> {
}


export interface IArrayShellState extends IShellState {
   readonly merges: number;
}


export interface IArrayUpdateEntry<T extends IId> {
   value: T;
   partial: T;
   index: number;
}

export interface IArrayLoad<T extends object> extends IObjectLoad<T> {
   packSize: number;
   position?: TPosition;
}
