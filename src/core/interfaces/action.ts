import { IId, TReadonlyArray } from '@taedr/utils';
import { AActionShell } from '../classes/actions/performers/core';
import { IShellState } from '../utils/state';
import { IObjectLoad, IShellCtx } from './shell';


export type TActionType = `ADD` | `UPDATE` | `DELETE`;
export type TActionsState = `INITIAL` | `LOADING` | `READY`;


export interface IPerform<T extends object> extends Omit<IObjectLoad<T>, 'isCheckSync' | 'isOnlyIfInitial'> {
   payload: T;
}

export interface IActionsShellCtx<T extends object, M extends object> {
   getPerformer(index: number): AActionShell<M, T>;
}


export interface ISyncActionsShellCtx<T extends object, M extends IId> extends IActionsShellCtx<T, M> {
   type: TActionType;
}
// ==================================================
//                   Ctx
// ==================================================
export interface IActionShellCtx<M extends object> extends Omit<IShellCtx<M>, 'cache'> {
}

export interface IObjectActionShellCtx<M extends object> extends IActionShellCtx<M> {
}


export interface IArrayActionShellCtx<M extends object> extends IActionShellCtx<M> {
}
// ==================================================
//                   Monitoring
// ==================================================
export interface IAction {
   config: IPerform<object>;
   state: IShellState;
}


export interface IActionError extends IAction {
}


export interface IActionPending<T> extends IAction {
   value: Promise<T>;
   abort(): void;
}


export interface IActionSuccess<T, M extends object> extends IAction {
   value: T;
   mapped: TReadonlyArray<M>;
}