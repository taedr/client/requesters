import { IBus } from '@taedr/reactive';
import { IObjectLoad } from './shell';
import { IChanges } from './sync';


export interface IRequest {
   readonly config: IObjectLoad<object>;
   resolve(data: any): void;
   reject(error: any): void;
}


export interface IRequester {
   readonly changes?: IBus<IChanges>;
   load(request: IRequest): void;
   abort(request: IRequest): void;
}