import { IId, TReadonlyArray } from '@taedr/utils';
import { IChange, TPosition } from '@taedr/arrays';
import { TShellState } from '../utils/state';
import { IShellData } from '../utils/data';

// ==================================================
//                   Core
// ==================================================
export interface ISynchronizable<T extends IId> extends IShellData<T> {
   syncUpdate(sync: IUpdateEvent<T>): void;
   syncAdd(sync: IAddEvent<T>): void;
   syncDelete(sync: IDeleteEvent<T>): boolean;
   getValueById(sync: IIdSync<T>): void;
   getRefsCount(sync: IRefsSync<T>): void;
}
// ==================================================
//              Sync
// ==================================================
export type TSyncAction = `ADD` | `UPDATE` | `DELETE`;
export type TSyncEvent = `REFS` | `ID` | `HASH` | TSyncAction;

export interface ISyncEvent {
   type: TSyncEvent;
   initiator: object;
   path: string[];
}

export interface IRefsSync<T extends IId> extends ISyncEvent {
   type: `REFS`;
   values: TReadonlyArray<T>;
   counts: number[];
}

export interface IIdSync<T extends IId> extends ISyncEvent {
   type: `ID`;
   id: string;
   value: T;
}


export interface IHashEntry {
   state: TShellState;
   value: object;
   merges: number;
}


export interface IHashSync<T extends IId> extends ISyncEvent {
   type: `HASH`;
   hash: number;
   entry: IHashEntry;
}
// ==================================================
//                Events
// ==================================================
export interface IAddEvent<T extends IId> {
   values: TReadonlyArray<T>;
   position: TPosition;
}

export interface IUpdateEvent<T extends IId> {
   values: TReadonlyArray<T>;
   syncked: T[];
}

export interface IDeleteEvent<T extends IId> {
   values: TReadonlyArray<IId>;
   deleted: Set<T>;
}
// ==================================================
//              Change
// ==================================================
export interface IChanges {
   added: IChange<IId>[];
   updated: IId[];
   deleted: IId[];
}
// ==================================================
//              Actions
// ==================================================
export interface IActionSyncEvent<T extends IId> extends ISyncEvent {
   type: TSyncAction;
   values: TReadonlyArray<T>;
}


export interface IUpdateSync<T extends IId> extends IActionSyncEvent<T> {
   type: `UPDATE`;
   syncked: T[];
}


export interface IAddSync<T extends IId> extends IActionSyncEvent<T> {
   type: `ADD`;
   position: TPosition;
}


export interface IDeleteSync<T extends IId> extends IActionSyncEvent<T> {
   type: `DELETE`;
   deleted: Set<T>;
}
