import { IId } from '@taedr/utils';
import { IObjectLoad, IShellCtx, ISyncShellCtx } from './shell';



export interface IObjectShellCtx<T extends object> extends IShellCtx<T> {
}


export interface IIdObjectShellCtx<T extends IId> extends ISyncShellCtx<T> {
 
}

export interface IIdObjectLoad<T extends object> extends IObjectLoad<T>, IId {
   
}