import { IReactive } from '@taedr/reactive';
import { TNew, IId } from '@taedr/utils';
import { TSUnknownKey } from '@taedr/jsontoclass/core/unknownKey';
import { IShellState, TShellState } from '../utils/state';
import { IRequester } from './request';
import { AShellMock } from '../utils/mock';
import { RequesterError } from '../utils/errors';
import { IObjectRequest } from '../utils/request';
import { ICacheCtx } from '../utils/cache';

export interface IShell<T> {
   readonly data: IReactive<T>
   readonly state: IShellState;
   load(config: IObjectLoad<object>): IObjectRequest<T>;
   clear(): void;
}

export interface IObjectLoad<T extends object> {
   payload?: T;
   path: string[];
   isCheckSync?: boolean;
   isOnlyIfInitial?: boolean;
}


export type TSCorruption = 'FORBID' | 'REPAIR';
export type TSStructure = 'FULL' | 'PARTIAL';

export type TModels<T extends object> = [TNew<T>, ...TNew<T>[]];

export interface IShellCtx<M extends object> {
   id: string;
   models: TModels<M>;
   ttlSec: number;
   requester: IRequester;
   mock?: AShellMock;
   isDebug?: boolean;
   SUnknownKeys?: TSUnknownKey;
   SCorruption?: TSCorruption;
   SStructure?: TSStructure;
   cache?: ICacheCtx;
}


export interface IShellEvent {
   id: string;
   state: TShellState;
   error?: RequesterError;
}


export interface ISyncShellCtx<M extends IId> extends IShellCtx<M> {

}