import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TaedrModule, getRoutes } from '@taedr/angular';
import { restSample, playground } from '../samples';
import { RequesterComponent } from './requester/requester.component';
import { RoutingService } from './services/routing.service';

// const [{ route, components }] = getRoutes({
//    requesters: {
//       playground: playground(),
//       rest: restSample(),
//       firebase: firebaseSample(),
//    },

// }, {
//    getTitle: title => title.split(/(?=[A-Z])/)[0],
// });


// export const REQUESTERS_ROUTE: Route = {

// };




@NgModule({
   imports: [
      CommonModule,
      RouterModule,
      // TaedrModule,
   ],
   declarations: [
      
     
   ],
   providers: [
      RoutingService
   ]
})
export class RequestersModule {

}
