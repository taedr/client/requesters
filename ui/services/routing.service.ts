import { Injectable, Query } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { adapter, REACTIVE, side } from '@taedr/reactive';
import { User } from '../../samples/firebase/model/user';

@Injectable()
export class RoutingService {
   readonly params = {
      route: adapter(this._route.params).pipe(REACTIVE, [
         side(params => {
            const id = `id` in params;
            console.log(`Route`, { params, id })
         })
      ], {}),
      query: adapter(this._route.queryParams).pipe(REACTIVE, [
         side(params => {
            const id = `id` in params;
            console.log(`Query`, { params, id })
         })
      ]),
   }
  

   constructor(
      private _router: Router,
      private _route: ActivatedRoute
   ) {
      this.watch();
   }

   public goToUser(user: User) {

      this._router.navigate([`requesters/firebase/user`, user.id])
   }


   protected watch() {
      this._route.params.subscribe(event => {
         console.log(`RT`, event)
      })

      this._router.events.subscribe(event => {
         // console.log(event)
      })
   }


   setAnchor() {
      this._router.navigate(
         [], 
         {
           relativeTo: this._route,
           queryParams: {}, 
           queryParamsHandling: 'merge', // remove to replace all query params by provided
         });
   }
}