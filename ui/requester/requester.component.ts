import { Component, Input } from '@angular/core';
import { TFunction } from '@taedr/utils';
import { ArrayShell, AShell } from '@taedr/requesters/core';


@Component({
   selector: 'app-requester',
   templateUrl: './requester.component.html',
   styleUrls: [
      './requester.component.scss'
   ]
})
export class RequesterComponent {
   public controls: [string, TFunction][] = [];
   public id: string;

   @Input(`requester`) set _requester(requester: AShell<any, object, object>) {
      this.id = requester.id;
      this.controls = [
         [`Reload`, () => requester.load(null)],
      ];
      

      if (requester instanceof ArrayShell) {
         this.controls.push([`Merge`, () => requester.merge(null)]);
      }
      
      this.controls.push(
         [`Clear`, () => requester.clear()],
      );
   }


}
 