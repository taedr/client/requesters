import { GComponent } from '@taedr/angular';
import { TFunction, TSMorph, getSyncMorph } from '@taedr/utils';
import { AShell } from '@taedr/requesters/core';


export function getComponent(
   requesters: TSMorph<void, AShell<object, object>[]>,
   actions:  Array<[string, TFunction]>
) {
   
   return new class extends GComponent {
      readonly template = `
         <section style="display: flex;">
            <app-requester *ngFor="let req of requesters"
                           [requester]="req"></app-requester>
         </section>
         <section style="display: flex;">
            <button *ngFor="let button of actions" (click)="button[1]()" class="extra">
               <span style="color: black;">{{ button[0] }}</span>
            </button>
         </section>
      `;
      readonly controller = class {
         readonly requesters = getSyncMorph(requesters, null);
         readonly actions = actions;
      }
   }
}
