import { JTC } from '@taedr/jsontoclass';

export class User {
   constructor(
      readonly id: string,
      readonly name: string,
      readonly age: number,
      readonly address: Address
   ) { }


   static [JTC.FILL] = () => new User(``, ``, 0, Address[JTC.FILL]());
}

export class Address {
   constructor(
      readonly country: string,
      readonly state: string,
   ) { }

   static [JTC.FILL] = () => new Address(``, ``);
}

export class UserFilter {
   constructor(
      public id: String,
   ) {}
}


export class UsersFilter {
   constructor(
      readonly ids: string[],
   ) {}
}

// export const user_1 = new User(`1`, `Max`, 21, new Address('Russia', 'SP'));
// export const user_2 = new User(`2`, `Mari`, 45, new Address('Ukraine', 'OD'));
// export const user_3 = new User(`3`, `Alex`, 31, new Address('USA', 'LA'));
// export const user_4 = new User(`4`, `Zander`, 26, new Address('Canada', 'GR'));
// export const user_5 = new User(`5`, `Casper`, 35, new Address('China', 'UH'));
// export const users = [user_1, user_2, user_3, user_4, user_5];