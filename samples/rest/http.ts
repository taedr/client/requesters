import { getComponent } from '../component';
import { Hub, FN_RANDOM_NUM, FN_MIRROR, TFunction, TConsumer } from '@taedr/utils';
import { UserListShell, UserItemShell } from './user/shell';
import { AShell, ObjectShell, ArrayShell, CorruptionError } from '@taedr/requesters/core';
import { User } from './user/model';
import { max } from '@taedr/arrays';


export function restSample() {
   const basic = new UserListShell(`Basic`, `users`);
   const second = new UserListShell(`Second`, `users`);
   const users_1 = new UserListShell(`users_1`, `users`);
   const users_2 = new UserListShell(`users_2`, `users`);
   const users_3 = new UserListShell(`users_3`, `users`);
   const users_4 = new UserListShell(`users_4`, `users`);
   const users_5 = new UserListShell(`users_5`, `users`);

   const user = new UserItemShell();

   // users.add.pending.on.


   // basic.

   // console.log(object);
   const hub = new Hub();

   AShell.on.all.node(hub).watch(event => {
      const parts = event.id.split(` `);


      // console.log(event, parts);
   })

   // const user = new ObjectShell({
   //    id: ``,
   //    model: User,
   //    requester: null,
   //    mock: null,
   //    ttlMs: 1000
   // })

   // user.state.value === ``
   // users.state.val


   // users.getById(2).


   function getMathc() {
      const count = max([basic.data.size, second.data.size], FN_MIRROR).value;

      const _bs = basic.data.value;
      const _sc = second.data.value;
      const match = {};

      for (let i = 0; i < count; i++) {
         const bs = _bs[i];
         const sc = _sc[i];

         if (bs !== undefined && sc !== undefined) {
            match[i] = bs === sc;
         }

      } 

      console.log(match);
   }


   async function goThrough(shell: UserListShell) {
      while (shell.state.is(`MERGABLE`)) {
         try {
            const values = await shell.load().value;
         } catch(e) {
            if (e instanceof CorruptionError) {
               const corrupted = e.result.data.corrupted as User[];

               for (const value of  corrupted) {
                  value.id
               }
            }
         }
      } 
   } 

   function getActions(shell: UserListShell): [string, TFunction][] {
      const id = shell.id;
      return [
         [`${id} reload`, async () => {
            const value = await shell.load().value;
            // const data = await shell.load().promise;
            // console.log(`Promise`, data);
            // getMathc();
            console.log(shell);

 

         }],
         [`${id} merge`, async () => {
            const request = shell.merge();
            const data = await request.value;

            console.log(shell);
            // shell.merge().
            // console.log(`Promise`, data);
            // getMathc();
         }],
         [`${id} clear`, async () => {
            shell.clear();
            console.log(shell);
         }],
         [`${id} load address`, async () => {
            shell.data.value[0]._address.load();

         }],
         [`${id} merge posts`, async () => {
            shell.data.value[0]._posts.merge();

         }],
      ]
   }


   return getComponent([] as any, [
      // ...getActions(basic),
      // ...getActions(second),
      ...getActions(users_1),
      ...getActions(users_2),
      ...getActions(users_3),
      // ...getActions(users_4),
      // ...getActions(users_5),
      [`add`, async () => {
         const { value, state } = basic.add(new User());
      }],
      [`delete`, async () => {
         const index = FN_RANDOM_NUM(0, basic.data.size - 1);
         const user = basic.data.get(index + ``);
         const { value, state } = basic.delete(user);
      }],
      [`update`, async () => {
         const { value, state } = basic.update({
            id: 1,
            avatar: `Avka`,
         });
      }],
      [`Get By Id`, async () => {
         user.load({ id: 1 });
         console.log(user);

      }],
      [`Promise`, async () => {

      }],
   ]);
}
