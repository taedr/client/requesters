var http = require('http');

var server = http.createServer(function (req, res) {
   res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
   res.setHeader('Access-Control-Allow-Headers', ['authorization', 'content-type']);
   res.setHeader('Access-Control-Allow-Methods', ['GET', 'POST']);


   if (req.method === 'OPTIONS') { // #Return#
      res.end();
      return true;
   }


   res.writeHead(200);
   res.end(JSON.stringify(null));
   // res.end(JSON.stringify([
   //    {
   //       id: 0,
   //       email: ``,
   //       first_name: ``,
   //       last_name: ``,
   //       avatar: ``,
   //    }
   // ]));
});




server.listen(3500, function () { console.log("Listening on port 3500") });

