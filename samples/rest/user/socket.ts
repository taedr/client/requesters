// import { IRequester, RequesterError, IRequest } from '@taedr/requesters/core';
// import { Bus, IBus } from '@taedr/reactive';
// import { TConsumer } from '@taedr/utils';

// export class AppSocket {
//    protected _socket: WebSocket = null;
//    protected _register = new Map<string, SocketRequester>()


//    // ---------------------------------------------
//    //                Api
//    // ---------------------------------------------
//    public connect(token: string) {
//       const url = `domen?token=${token}`;
//       const socket = this._socket = new WebSocket(url);

//       socket.onopen = () => null;
//       socket.onmessage = message => this.onMessage(message);
//       socket.onclose = event => this.onClose(event);
//    }


//    public close() {
//       const message = `Client closed connection`;
//       const error = new RequesterError(2, message);

//       this._socket.close(1000, JSON.stringify({ message }));

//    }


//    public getRequester(id: string): IRequester {
//       const requester = new SocketRequester(id);
//       this._register.set(id, requester);
//       return requester;
//    }
//    // ---------------------------------------------
//    //                Internal
//    // ---------------------------------------------
//    protected send(payload: object) {
//       const message = JSON.stringify(payload);
//       this._socket.send(message);
//    }


//    protected onClose({ code, reason }: CloseEvent) {
//       if (code === 1000) return; // #Return#
//       const mess = code === 1006 ? 'Closed externaly' : reason;
//       const error = new RequesterError(code, mess);

//       this.sendError(error);
//    }


//    protected async onMessage(message: MessageEvent) {
//       try {
//          const data = JSON.parse(message.data);
//          this.sendData(data);
//       } catch (e) {
//          const error = new RequesterError(2, e.message);
//          return this.sendError(error);
//       }
//    }


//    protected onError(e: Event) {

//    }


//    protected sendData(error: RequesterError) {
//       this._register.forEach(entry => entry.error.send(error));
//    }


//    protected sendError(error: RequesterError) {
//       this._register.forEach(entry => entry.error.send(error));
//    }
// }



// export class SocketRequester implements IRequester {
//    readonly data = new Bus<object>();
//    readonly error = new Bus<RequesterError>();

//    constructor(
//       readonly load: TConsumer<IRequest>,
//       readonly expired: TConsumer<IRequest>,
//       readonly clear: TConsumer<string>,
//    ) { }
// }

