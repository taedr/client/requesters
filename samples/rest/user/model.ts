import { JTC } from '@taedr/jsontoclass';
import { AddressShell } from './address/shell';
import { PostsShardShell } from './post/shells/shard';
import { Post } from './post/model';

export const COMMENTS = Symbol(`COMMENTS`);


const ADDRESS = Symbol(`ADDRESS`);
const POSTS = Symbol(`POSTS`);

export const USERS_IN_PACK = 6;


export abstract class AUserComposite {
   public id = 0;
   // ---------------------------------------------
   //                Access
   // ---------------------------------------------
   public get _address() { return this[ADDRESS]; }
   public get _posts() { return this[POSTS]; }
   // ---------------------------------------------
   //                Symbols
   // ---------------------------------------------
   private [ADDRESS]: AddressShell;
   private [POSTS]: PostsShardShell;
   // ---------------------------------------------
   //                Predefined
   // ---------------------------------------------
   // private posts: Post[] = [];
   // ---------------------------------------------
   //                Initer
   // ---------------------------------------------
   static [JTC.INIT](user: AUserComposite) {
      
      user[ADDRESS] = new AddressShell(user.id);
      user[POSTS] = new PostsShardShell(user.id, [] /* user.posts */);
      // delete user.posts;
   }


   // static [JTC.FILL](user: AUserComposite) {
   //    user.posts = [new Post()];
   // }
}

export class User extends AUserComposite {
   public email = ``;
   public first_name = ``;
   public last_name = ``;
   public avatar = ``;

   @JTC.null public age = 0;
}


export interface IUsersFilter {
   page: number;
}





export interface IAddUser {
   path: string;
   payload: User;
}


export interface IDeleteUser {
   path: string;
   id: number;
}


export interface IUpdateUser {
   path: string;
   payload: Partial<User>;
}