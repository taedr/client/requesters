export class Post {
   public id = 0;
   public text = ``;
}


export interface IPostForm {
   text: string;
}


export interface IPostAdd extends IPostForm {
   userId: number;
}



export interface IPostDelete {
   id: number;
}