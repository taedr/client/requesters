import { AShellMock } from '@taedr/requesters/core';
import { Post } from './model';
import { FN_PACK, IdNum, IId } from '@taedr/utils';
import { IFilter } from '../../core/filter';

export class PostsStub extends AShellMock {
   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData({ page }: IFilter, path: string) {
      const userId = +path.split(`/`).slice(-2)[0];
      const startIndex = page * 5;

      const posts = FN_PACK(5).map(id => {
         const post = new Post();
         post.id = startIndex + id;
         return post;
      });


      return posts;
   }
}


export class PostAddStub extends AShellMock {
   protected _id = 0;

   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData(_: object, path: string) {
      const userId = +path.split(`/`).slice(-2)[0];
      const post = new Post();
      post.id = --this._id;
      return post;
   }
}


export class PostDeleteStub extends AShellMock {
   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData(_: object, path: string) {
      const userId = +path.split(`/`).slice(-2)[0];

      return { id: 1 };
   }
} 