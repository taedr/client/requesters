import { APostsShell } from './shell';
import { Post, IPostAdd, IPostForm } from '../model';
import { POST_VALUES_SYNC } from '../sync';
import { POST_ACTIONS, POST_ACTIONS_SYNC } from '../actions';

import { PostsStub } from '../stub';



export class PostsShardShell extends APostsShell {
   constructor(
      protected _userId: number,
      initial: Post[],
   ) {
      super({
         id: `User ${_userId} Post Shard`,
         model: Post,
         path: filter => ({ path: `users/${_userId}/posts`, payload: filter }),
         packSize: 5,
         requester: null,
         ttlSec: 2,
         mock: new PostsStub(true),
         isDebug: true,
         // shardId: _userId + ``,
         actions: POST_ACTIONS_SYNC
      }, initial);
   }


   public add(form: IPostForm) {
      return POST_ACTIONS.add.perform({
         ...form,
         userId: this._userId,
      }, /* this._userId + `` */);
   }


   public delete(post: Post) {
      return POST_ACTIONS.delete.perform(post, /* this._userId + `` */);
   }
} 
