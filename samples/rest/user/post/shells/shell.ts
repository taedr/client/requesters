import { ArrayShell, IActionPending } from '@taedr/requesters/core';
import { IFilter } from '../../../core/filter';
import { Post, IPostAdd } from '../model';
import { IdNum } from '@taedr/utils';

export abstract class APostsShell extends ArrayShell<IFilter, Post> {
   // ---------------------------------------------
   //                Abstract
   // ---------------------------------------------
   public abstract add(post: IPostAdd): IActionPending<IPostAdd, Post>;
   public abstract delete(post: Post): IActionPending<Post, IdNum>;
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public load(filter: IFilter = { page: 1 }) {
      return super.load(filter);
   }

   public merge() {
      return super.merge({
         page: this.state.merges + 1,
      });
   }  
}