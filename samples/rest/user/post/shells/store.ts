import { APostsShell } from './shell';
import { IPostAdd, IPostForm, Post } from '../model';
import { POST_VALUES_SYNC } from '../sync';
import { POST_ACTIONS, POST_ACTIONS_SYNC } from '../actions';
import { PostsStub } from '../stub';


export class PostsStoreShell extends APostsShell {
   constructor() {
      super({
         id: `Post Store`,
         model: Post,
         path: filter => ({ path: `posts`, payload: filter }),
         packSize: 5,
         requester: null,
         ttlSec: 2,
         mock: new PostsStub(true),
         isDebug: true,
         actions: POST_ACTIONS_SYNC
      });
   }


   public add(form: IPostAdd) {
      return POST_ACTIONS.add.perform(form);
   }


   public delete(post: Post) {
      return POST_ACTIONS.delete.perform(post);
   }
}  