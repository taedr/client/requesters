import { ObjectAction, ShellActionsSync } from '@taedr/requesters/core';
import { IdNum, FN_MIRROR, TDeepPartial, IId } from '@taedr/utils';
import { Post, IPostAdd, IPostDelete } from './model';
import { PostAddStub, PostDeleteStub } from './stub';
import { map } from '@taedr/reactive';
import { TAIL } from '@taedr/arrays';


export const POST_ACTIONS = {
   add: new ObjectAction<IPostAdd, Post>(() => ({
      id: `User Add`,
      model: Post,
      ttlSec: 2,
      isDebug: true,
      mock: new PostAddStub(true),
      requester: null,
      path: `posts/add`,
   })),
   delete: new ObjectAction<Post, IdNum>(() => ({
      id: `User Delete`,
      model: IdNum,
      ttlSec: 2,
      isDebug: true,
      mock: new PostDeleteStub(true),
      requester: null,
      path: ({ id }) => ({ path: `posts/${id}`, payload: null }),
   })),
   share: new ObjectAction<IPostAdd, TDeepPartial<Post>>(() => ({
      id: `Post Share`,
      model: Post,
      ttlSec: 2,
      SEmpties: `PARTIAL`,
      isDebug: true,
      mock: new PostAddStub(true),
      requester: null,
      path: `post/share`,
   })),
}


export const POST_ACTIONS_SYNC = new ShellActionsSync<Post>({
   // add: POST_ACTIONS.add.success.pipe(({  }) =>  ),
   delete: POST_ACTIONS.delete.success
}); 