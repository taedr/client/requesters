import { ActionsShell, SyncActionsShell, IPerform, ObjectActionShell } from '@taedr/requesters/core';
import { User, IAddUser, IDeleteUser, IUpdateUser } from './model';
import { UserAddStub, UserDeleteStub, UserUpdateStub } from './stub';
import { ReqResApi } from '../core/reqRes';
import { map, latest } from '@taedr/reactive';
import { TAIL } from '@taedr/arrays';
import { IdNum, FN_MIRROR, TDeepPartial, IId } from '@taedr/utils';
import { POST_ACTIONS } from './post/actions';

export const USER_ACTIONS = {
   add: new ObjectAction<IAddUser, User>(() => ({
      id: `User Add`,
      model: User,
      ttlSec: 2,
      isDebug: true,
      mock: new UserAddStub(true),
      requester: new ReqResApi(`POST`),
      path: FN_MIRROR,
   })),
   delete: new ObjectAction<IDeleteUser, IId>(() => ({
      id: `User Delete`,
      model: IdNum,
      ttlSec: 2,
      isDebug: true,
      mock: new UserDeleteStub(true),
      requester: new ReqResApi(`POST`),
      path: ({ path, id }) => ({ path: `${path}/${id}`, payload: null }),
   })),
   update: new ObjectAction<IUpdateUser, User>(() => ({
      id: `User Update`,
      model: User,
      ttlSec: 2,
      SEmpties: `PARTIAL`,
      isDebug: true,
      mock: new UserUpdateStub(true),
      requester: new ReqResApi(`POST`),
      path: FN_MIRROR,
   })),
   like: new ObjectAction<IUpdateUser, User>(() => ({
      id: `User Like`,
      model: User,
      ttlSec: 2,
      SEmpties: `PARTIAL`,
      isDebug: true,
      mock: new UserUpdateStub(true),
      requester: new ReqResApi(`POST`),
      path: FN_MIRROR,
   })),
}


export interface IUserAdd extends IPerform<User> {
}





export class UserAdd extends SyncActionsShell<IUserAdd, Readonly<User>, User> {
   constructor(
   ) {
      super({
         type: `ADD`,
         getPerformer: index => new ObjectActionShell<IUserAdd, User>({
            id: `User Add ${index}`,
            models: [User],
            ttlSec: 5,
            requester: new ReqResApi(`POST`),
         })
      })
   }

   public perform({ payload }: { payload: User }) {
      return super.perform({
         path: { users: null },
         payload
      });
   }
}

export interface IUserUpdate extends IPerform<User> {
}

export class UserUpdate extends SyncActionsShell<IUserUpdate, Readonly<User>, User> {
   constructor(
   ) {
      super({
         type: `UPDATE`,
         getPerformer: index => new ObjectActionShell<IUserUpdate, User>({
            id: `User Add ${index}`,
            models: [User],
            requester: new ReqResApi(`POST`),
            ttlSec: 5,
            SStructure: `PARTIAL`,
         })
      })
   }

   public perform({ payload }: { payload: User }) {
      return super.perform({
         path: { users: null },
         payload
      });
   }
}






const USER_ADD = new UserAdd();

// USER_ADD.pending.get(0).