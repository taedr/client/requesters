import { ObjectShell } from '@taedr/requesters/core';
import { Address } from './model';
import { AddressStub } from './stub';

export class AddressShell extends ObjectShell<null, Address> {
   constructor(
      userId: number
   ) {
      super({
         id: `User ${userId} Address`,
         model: Address,
         path: () => ({ path: `users/${userId}/address`, payload: null }),
         requester: null,
         ttlSec: 2,
         mock: new AddressStub(true),
         isDebug: true,
      });
   }

   public load() {
      return super.load(null);
   }
} 