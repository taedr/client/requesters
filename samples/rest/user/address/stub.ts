import { AShellMock } from '@taedr/requesters/core';
import { Address } from './model';

export class AddressStub extends AShellMock {
   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData(_: object, path: string) {
      const userId = +path.split(`/`).slice(-2)[0];
      // const user = usersStub.get(({ id }) => id === userId);

      // if (!user) {
      //    throw new Error(`Didn't find user with such id`);
      // }

    

      return new Address();
   }
}