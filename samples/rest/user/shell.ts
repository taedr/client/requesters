import { User, IUsersFilter, USERS_IN_PACK } from './model';
import { UsersStub, UserStub } from './stub';
import { ReqResApi } from '../core/reqRes';
import { ArrayShell, IdObjectShell, IRequest, ObjectShell, ERRORS, RequesterError, ArrayCache, TShellState, IArrayLoad } from '@taedr/requesters/core';
import { USER_ACTIONS, USER_ACTIONS_SYNC } from './actions';
import { IdNum, IId } from '@taedr/utils';
import { USER_VALUES_SYNC } from './sync';
import { JTC } from '@taedr/jsontoclass';
import { HEAD } from '@taedr/arrays';

const isDebug = true;
const isStub = false;


export class Responce {
   page = 0;
   per_page = 0;
   total = 0;
   total_pages = 0;
   ad = new ResponceAd();
}

export class ResponceAd {
   public company = ``;
   public url = ``;
   public text = ``;
}


export class UserListShell extends ArrayShell<IArrayLoad<IUsersFilter>, User> {
   // protected _additional: Responce;
   
   constructor(
      id: string,
   ) {
      super({
         id: `${id} Users`,
         models: [User],
         isDebug,
         ttlSec: 3,
         mock: null,
         cache: { size: 3, entryTtlSec: 30 },
         requester: new ReqResApi(`GET`, resp => this.mapResponce(resp)),
      });
   } 
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public load() {
      return super.load({ 
         packSize: 6,
         path: { 
            users: null
         },
         payload: {
            page: 1
         }
       });
   }

 
   public merge() {
      return super.load({ 
         packSize: 6,
         payload: {
            page: this.state.merges + 1,
         }
       });
   }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   protected mapResponce(responce: object): object[] {
      const data = responce['data'] ?? [];
      delete responce['data'];
      const resp = JTC.convert(Responce, responce, { isFreeze: true });

      if (resp.isCorrupted) {
         throw new Error(`Not valid additional info`);
      }

      // this._additional = resp.data.first;
      console.log(resp)

      return data;
   }


   // protected getState(data: User[]): TShellState {
   //    let state = super.getState(data);
   //    const isFull = this._additional?.total_pages === this._state.merges;
   //    if (state === `READY` && isFull) state = `FULL`;
   //    return state;
   // }
}



export class UserItemShell extends IdObjectShell<IdNum, User> {
   constructor(
   ) {
      super({
         id: `User`,
         model: User,
         path: ({ id }) => ({ path: [`users`, id ], payload: null }),
         requester: new ReqResApi(`GET`),
         ttlSec: 2,
         mock: new UserStub(isStub),
         isDebug,
         actions: USER_ACTIONS_SYNC,
      });
   }
}