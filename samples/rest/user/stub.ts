import { AShellMock } from '@taedr/requesters/core';
import { User, IUsersFilter, IAddUser, IDeleteUser, IUpdateUser, USERS_IN_PACK } from './model';
import { FN_PACK, IId, FN_GET_ID, TDeepPartial } from '@taedr/utils';
import { Collection } from '@taedr/reactive';
import { max } from '@taedr/arrays';
import { Post } from './post/model';



// const usersStub = (function () {
//    const count = 10000;
//    const users: User[] = [];

//    for (let i = 0; i < count; i++) {
//       const user = new User();
//       user.id = i;
//       users.push(user);
//    }

//    return new Collection<User>(users, FN_GET_ID);
// })();


export class UsersStub extends AShellMock {
   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData(payload: IUsersFilter) {
      const from = (payload.page - 1) * USERS_IN_PACK;
      const to = from + USERS_IN_PACK;
      // const slice = usersStub.value.slice(from, to);

      console.log(payload)

      const slice = FN_PACK(USERS_IN_PACK, from).map(id => {
         const user = new User();
         user.id = id;
         user['posts'] = FN_PACK(5).map(id => {
            const post = new Post();
            post.id = id;
            return post;
         });
         return user;
      })

      return slice;
   }
}


export class UserStub extends AShellMock {
   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData(_, path: string[]) {
      const userId = +path.slice(-1)[0];
      // const user = usersStub.get(({ id }) => id === userId);

      // if (!user) {
      //    throw new Error(`Didn't find user with such id`);
      // }

      const user = new User();
      user.id = userId;

      return user;
   }
}



export class UserAddStub extends AShellMock {
   protected static _id = 0;
   
   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData(user: User) {
      if (!!user.id) {
         throw new Error(`User can't have setted id while adding`);
      }

      // user.id = max(usersStub.value, ({ id }) => id).max + 1;
      // usersStub.add(user);

      // return user;
      user.id = --UserAddStub._id;

      return user;
   }
}

export class UserDeleteStub extends AShellMock {
   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData(_: never, path: string[]) {
      const id = +path.slice(-1)[0];
      // const [user] = usersStub.delete.cross(FN_GET_ID, { id });

      // if (user) {
      //    return user.value;
      // } else {
      //    throw new Error(`Didn't find user with such id`);
      // }

      return { id };
   }
}



export class UserUpdateStub extends AShellMock {
   constructor(isActive: boolean) {
      super(isActive, 100);
   }

   protected async getData(user: User): Promise<User> {
      // const curr = usersStub.get(({ id }) => id === user.id);

      // return {
      //    id: curr.id,
      //    age: curr.age += 1,
      // };

      return user;
   }
}