import { HttpRequester } from '@taedr/requesters/rest';
import { TRequestMethod, FN_MIRROR } from '@taedr/utils';

export class ReqResApi extends HttpRequester {
   constructor(
      method: TRequestMethod,
      protected _unwrap: (responce: object) => object = FN_MIRROR,
   ) {
      super({
         domain: `https://reqres.in/api`,
         method,
      });
   }


   protected async getJson(responce: Response) {
      const json = await super.getJson(responce);
      const unwraped = this._unwrap(json);
      return unwraped;
   }
}


